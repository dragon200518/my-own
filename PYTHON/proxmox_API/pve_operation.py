import requests
import time
import json
import paramiko

pve_ip = "172.17.72.44:8096"

url_base_json = "https://{}/api2/json".format(pve_ip)

SHARE_CONFIG_PATH='/var/share/ezfs/shareroot/share_config'

SSH_USER = 'root'
SSH_PASSWORD = 'p@ssw0rd'

#header,session = login(pve_ip)

session = requests.Session()
def login(ip):

    try:

        # session = requests.Session()
        #url_base_extjs = "https://{}/api2/extjs".format(ip)
        url_base_json = "https://{}/api2/json".format(ip)

        parameters = {"username": "root", "password": "p@ssw0rd", "realm": "pam"}
        url_login = "{}/access/ticket".format(url_base_json)

        resp = session.post(url_login, data=parameters, verify=False)
        token = resp.json()['data']['CSRFPreventionToken']
        cookie = resp.json()['data']['ticket']
        header = {
            'Cookie': "PVEAuthCookie={}".format(cookie),
            'CSRFPreventionToken': token
        }
        session.headers.update(header)
        
    except Exception as e:
        print("exception happen login, exception is :{}".format(e))
    
# header,session = login(pve_ip)
def request(method, *args, **kwargs):

    try:
        resp = session.request(method, *args, **kwargs)
        #print("session header is :{}, resp :{}".format(session.headers,resp))
        # resp.raise_for_status()
        if resp.status_code == 401:
            login(pve_ip)
            resp = session.request(method, *args, **kwargs)
            print("wsg debug ,ticket is out of date, now login again, response is :{}".format(resp))
        return resp
    except Exception as e:
        print("exception happen when request, exception is :{}".format(e))

def clone_vm(vmid, host_of_vm, new_vm_id, new_vm_name, target_host):
#if clone ok will return the clone task 
    url_clone_vm = "{}/nodes/{}/qemu/{}/clone".format(url_base_json,host_of_vm,vmid)
    #header,session = login(ip)  
    post_data={
        "newid": new_vm_id,
        "name": new_vm_name,
        "target": target_host,
        "full": 1
    }
    try:
        #response = session.post(url_clone_vm, headers=header,data=post_data,verify=False)
        response = request('post',url_clone_vm,data=post_data,verify=False)
        return_code = response.status_code
        if return_code == 200:
            return response.json()['data']
        else:
            print("url :{} ,request not ok, return code is :{},reason is :{}".format(url_clone_vm,return_code,response.reason) )
            return False
    except Exception as e:
        print("exception happen when do clone vm,exception is {}".format(e))
        

def clone_vm_progress(taskid,host_of_vm):
    url_clone_status="{}/nodes/{}/tasks/{}/status".format(url_base_json,host_of_vm,taskid)
    #header,session = login(ip)
    try:
        #print(url_clone_status)
        #response = session.get(url_clone_status, headers=header,verify=False)
        response = request('get',url_clone_status,verify=False)
        return_code = response.status_code
        if return_code == 200:
            return response.json()['data']['status']
        else:
            print("url :{},request not ok, return code is {},reason is :{}".format(url_clone_status,return_code,response.reason) )
            return False
    except Exception as e:
        print("exception happen when check vm clone progress,exception is {}".format(e))

def get_vm_nic_config(new_vm_id,target_host):
    
    ## return nic_config like,[{'net1': 'e1000=9A:8D:95:F4:15:69,bridge=vmbr0,firewall=1'},{'net0': 'virtio=8A:D7:72:F3:AA:61,bridge=vmbr1,firewall=1,tag=25'}]

    nic_config=[]

    url_get_vm_config="{}/nodes/{}/qemu/{}/pending".format(url_base_json,target_host,new_vm_id)
    try:
        #response = session.get(url_get_vm_config, headers=header,verify=False)
        response = request('get',url_get_vm_config,verify=False)
        if response.status_code == 200:
            vm_config=response.json()['data']
            for item in vm_config:
                each_nic={}
                    #print(item['key'])
                if 'net' in item['key']:
                    #print(item['value'],item['key'])
                    each_nic[item['key']]=item['value']
                    nic_config.append(each_nic)
            return nic_config
        else:
            print("url :{},request not ok, return code is {},reason is :{}".format(url_get_vm_config,response.status_code,response.reason) )
            return False
    except Exception as e:
        print("exception happen when get vm nic config,exception :{}".format(e))

def nic_config_to_dict(config_str):
    ## return like  {'firewall': '1', 'virtio': '96:0E:F7:F4:DC:B9', 'bridge': 'vmbr0'}
    config_dict={}
    config_list=config_str.split(',')
    for each in config_list:
        print("debug,in nic config to dict :each {}".format(each))
        #print(each.split('='))
        kv=each.split('=')
        config_dict[kv[0]]=kv[1]
    return config_dict

def nic_config_dict_to_str(config_dict):
    config_list=[]
    for k,v in config_dict.items():
        str='{}={}'.format(k,v)
        config_list.append(str)  
    #print(config_list)
    config_str=','.join(config_list)
    return config_str





def config_vm_old(new_vm_id,target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway):
    #url_vm_config='https://172.17.72.45:8096/api2/extjs/nodes/hci01/qemu/300/config'
    #nic is like net0,net1
    # this funtion will create ip config in share-folder,

    target_nic_config={}

    print("begin config: nic {}".format(nic))
    if nic and vlan_tag and new_ip and new_netmask and new_gateway:
        nic_config=get_vm_nic_config(new_vm_id,target_host)
        print(nic_config)
        if nic_config:
            for each_nic in nic_config:
                print("debug in nic_config ,each_nic :{}".format(each_nic))
                if nic in each_nic.keys():
                    ## vlan should put in this nic
                    nic_config_dict=nic_config_to_dict(each_nic[nic])
                    print(nic_config_dict['e1000'])

                    ## now generate file use mac address
                    ip_config_file="{}/{}.conf".format(SHARE_CONFIG_PATH,nic_config_dict['e1000'].replace(':', '-'))
                    ip_config_content={}
                    ip_config_content['nic']=nic
                    ip_config_content['ip']=new_ip
                    ip_config_content['netmask']=new_netmask
                    ip_config_content['gateway']=new_gateway

                    print(ip_config_file)
                    try:
                        with open(ip_config_file,'w') as f:
                            json.dump(ip_config_content,f)
                            print("ip config write to file ,ok")
                    except Exception as e:
                        print("exception happen when generate ip config file")
                        continue
                    #now create vlan tag config, in nic
                    if 'tag' in nic_config_dict.keys():
                        #have tag before,now change
                        nic_config_dict.update({'tag':vlan_tag})
                    else:
                        #no tag before,now add
                        nic_config_dict['tag']=vlan_tag
                    target_nic_config[nic] = nic_config_dict_to_str(nic_config_dict)
                    #print('target nic config is {}'.format(target_nic_config))
        #because have vlan tag,should set ip first, so now boot vm
        if start_vm(new_vm_id,target_host):
            print("start vm ok, now vm ip should have configured. so begin shutdown")
            time.sleep(5)
            if stop_vm(new_vm_id,target_host):
                print("stop vm ok, now can config vm")

        
    else:
        print('nic config info is not sufficient, so will not config nic ')
        return


    #



    post_data={}
    if target_nic_config is None and new_vm_storage is None:
        post_data['sockets']=new_vm_socket
        post_data['cores']=new_vm_cores
        post_data['memory']=new_vm_memory

    elif target_nic_config is None:
        post_data['sockets']=new_vm_socket
        post_data['cores']=new_vm_cores
        post_data['memory']=new_vm_memory
        post_data['scsi1']=new_vm_storage

    else:
        post_data['sockets']=new_vm_socket
        post_data['cores']=new_vm_cores
        post_data['memory']=new_vm_memory
        post_data['scsi1']=new_vm_storage
        post_data.update(target_nic_config)

    url_vm_config="{}/nodes/{}/qemu/{}/config".format(url_base_json,target_host,new_vm_id)

    try:
        #header,session = login(ip)
        print("post data is: {}".format(post_data))
        response = session.put(url_vm_config, headers=header,data=post_data,verify=False)
        if response.status_code ==200:
            return True
        else:
            print("url :{},request not ok, return code is {},reason is :{}".format(url_vm_config,response.status_code,response.reason) )
            return False
    except Exception as e:
        print("exception happen when config vm")


def ssh_do_cmd(dest_host,cmd):
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(dest_host, 22, SSH_USER, SSH_PASSWORD)
        stdin,stdout, stderr = ssh.exec_command(cmd)
        output = stdout.read().decode('utf-8')    # use utf-8
        ssh.close()
        return output
    except Exception as e:
        print('exception when ssh_do_cmd: {}'.format(str(e)))
        return False

def check_vm_up(vmid,host_of_vm):
    try:
        cmd='qm guest exec {} hostname'.format(vmid)
        result=json.loads(ssh_do_cmd(host_of_vm,cmd))
        if result['exitcode'] == 0:
            print("vm is running")
            return True
        else:
            return False
    except Exception as e:
        print('exception when check vm up : {}'.format(str(e)))

def config_windows_vm_ip(vmid,host_of_vm,nic_name,new_ip,new_netmask,new_gateway):
    """
    need ssh host_of_vm  to do command qm guest exce
    host_of_vm is hostname, make sure hostname could ssh
    qm guest exec 110 netsh interface ip set address name="net0" static 172.17.73.158 255.255.252.0 172.17.75.254
    """
    cmd='qm guest exec {} netsh interface ip set address name="{}" static {} {} {}'.format(vmid,nic_name,new_ip,new_netmask,new_gateway)
    try:
        # windows boot need time,so sleep 30s
        while not check_vm_up(vmid,host_of_vm):
            time.sleep(2)
            print("vm :{} is not up, sleep 2s".format(vmid))
        result=json.loads(ssh_do_cmd(host_of_vm,cmd))
        if result['exitcode'] == 0:
            print("vm ip set ok")
            return True
    except Exception as e:
        print("exception happen when try set windows vm ip,exception :{}".format(e))
    


def config_vm(new_vm_id,target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway):
    #url_vm_config='https://172.17.72.45:8096/api2/extjs/nodes/hci01/qemu/300/config'
    #nic is like net0,net1
    target_nic_config={}
    print("begin config: nic {}".format(nic))
    if nic and vlan_tag:
        nic_config = get_vm_nic_config(new_vm_id,target_host)
        print(nic_config)
        if nic_config:
            for each_nic in nic_config:
                print("debug in nic_config ,each_nic :{}".format(each_nic))
                if nic in each_nic.keys():
                    ## vlan should put in this nic
                    nic_config_dict=nic_config_to_dict(each_nic[nic])
                    #print(nic_config_dict['e1000'])

                    if 'tag' in nic_config_dict.keys():
                        #have tag before,now change
                        nic_config_dict.update({'tag':vlan_tag})
                    else:
                        #no tag before,now add
                        nic_config_dict['tag']=vlan_tag
                    target_nic_config[nic] = nic_config_dict_to_str(nic_config_dict)
                    #print('target nic config is {}'.format(target_nic_config))
        #because have vlan tag,should set ip first, so now boot vm
        # if start_vm(new_vm_id,target_host):
        #     print("start vm ok, now vm ip should have configured. so begin shutdown")
        #     time.sleep(5)
        #     if stop_vm(new_vm_id,target_host):
        #         print("stop vm ok, now can config vm")      
    else:
        print('nic config info is not sufficient, so will not config nic ')

    post_data={}
    if target_nic_config is None and new_vm_storage is None:
        post_data['sockets']=new_vm_socket
        post_data['cores']=new_vm_cores
        post_data['memory']=new_vm_memory

    elif target_nic_config is None:
        post_data['sockets']=new_vm_socket
        post_data['cores']=new_vm_cores
        post_data['memory']=new_vm_memory
        post_data['scsi1']=new_vm_storage

    else:
        post_data['sockets']=new_vm_socket
        post_data['cores']=new_vm_cores
        post_data['memory']=new_vm_memory
        post_data['scsi1']=new_vm_storage
        post_data.update(target_nic_config)

    url_vm_config="{}/nodes/{}/qemu/{}/config".format(url_base_json,target_host,new_vm_id)

    try:
        #header,session = login(ip)
        print("post data is: {}".format(post_data))
        #response = session.put(url_vm_config, headers=header,data=post_data,verify=False)
        response = request('put',url_vm_config,data=post_data,verify=False)
        if response.status_code ==200:
            return True
        else:
            print("url :{},request not ok, return code is {},reason is :{}".format(url_vm_config,response.status_code,response.reason) )
            return False
    except Exception as e:
        print("exception happen when config vm,exception is {}".format(e))

def stop_vm_progress(taskid,target_host):
    url_stop_status="{}/nodes/{}/tasks/{}/status".format(url_base_json,target_host,taskid)

    try:
        #response = session.get(url_stop_status, headers=header,verify=False)
        response = request('get',url_stop_status,verify=False)
        if response.status_code ==200:
            return response.json()['data']['status']
        else:
            print("url :{},request not ok, return code is {},reason is :{}".format(url_stop_status,response.status_code,response.reason) )
            return False
    except Exception as e:
        print("exception happen when check vm stop progress,exception is {}".format(e))

def stop_vm(vmid,target_host):    
    url_stop_vm = "{}/nodes/{}/qemu/{}/status/stop".format(url_base_json,target_host,vmid)
    try:
        #response = session.post(url_stop_vm, headers=header,verify=False)
        response = request('post',url_stop_vm,verify=False)
        if response.status_code ==200:
            taskid=response.json()['data']
            stop_status=stop_vm_progress(taskid,target_host)
            if stop_status =='running':
                while True:
                    print("i am waiting for vm to stop")
                    time.sleep(2)
                    stop_status=stop_vm_progress(taskid,target_host)
                    if stop_status== 'stopped':
                        break         
            return True  
        else:
            print("url :{},request not ok, return code is {},reason is :{}".format(url_stop_vm,response.status_code,response.reason) )
    except Exception as e:
        print("exception happen when stop vm ,exception is {}".format(e))
        

def start_vm_progress(taskid,target_host):
    url_start_status="{}/nodes/{}/tasks/{}/status".format(url_base_json,target_host,taskid)
    try:
        #response = session.get(url_start_status, headers=header,verify=False)
        response = request('get',url_start_status,verify=False)
        if response.status_code ==200:
            return response.json()['data']['status']
        else:
            print("url :{},request not ok, return code is {},reason is :{}".format(url_start_status,response.status_code,response.reason) )
            return False
    except Exception as e:
        print("exception happen when check vm start progress,exception is {}".format(e))

def start_vm(new_vm_id,target_host):
    url_start_vm = "{}/nodes/{}/qemu/{}/status/start".format(url_base_json,target_host,new_vm_id)
    try:
        #header,session = login(ip)
        #response = session.post(url_start_vm, headers=header,verify=False)
        response = request('post',url_start_vm,verify=False)
        if response.status_code ==200:
            taskid=response.json()['data']
            start_status=start_vm_progress(taskid,target_host)
            if start_status =='running':
                while True:
                    print("i am waiting for vm to start")
                    time.sleep(2)
                    start_status=start_vm_progress(taskid,target_host)
                    if start_status== 'stopped':
                        break
            return True
    except Exception as e:
        print("exception happen when start vm, exception is ,exception is {}".format(e))




def clone_and_modify_vm(vmid, host_of_vm, new_vm_id, new_vm_name, target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway):

    taskid =clone_vm(vmid,host_of_vm,new_vm_id,new_vm_name,target_host)
    clone_status=clone_vm_progress(taskid,host_of_vm)
    if clone_status =='running':
        while True:
            print("clone running, i am waiting for clone finish")
            time.sleep(2)
            clone_status=clone_vm_progress(taskid,host_of_vm)
            if clone_status== 'stopped':
                break
    
    print("clone finish,do sth")

    if config_vm_old(new_vm_id,target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway): 
        print("config vm ok, now begin boot vm")
        if start_vm(new_vm_id,target_host):
            print("start vm ok, now config vm ip")
            return "ok"



def clone_and_modify_vm2(vmid, host_of_vm, new_vm_id, new_vm_name, target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway):   
    taskid =clone_vm(vmid,host_of_vm,new_vm_id,new_vm_name,target_host)
    clone_status=clone_vm_progress(taskid,host_of_vm)
    if clone_status =='running':
        while True:
            print("clone running, i am waiting for clone finish")
            time.sleep(2)
            clone_status=clone_vm_progress(taskid,host_of_vm)
            if clone_status== 'stopped':
                break
    
    print("clone finish,begin config vm")
    if config_vm(new_vm_id,target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway): 
        print("config vm ok, now begin boot vm")
        if start_vm(new_vm_id,target_host):
            print("start vm ok, now config vm ip")
            if new_ip and new_netmask and new_gateway:
                ## need set nic ip info
                if config_windows_vm_ip(new_vm_id,target_host,nic,new_ip,new_netmask,new_gateway):
                    return "ok"
            else:
                # no nic ip info, will not set ip
                return "ok"

