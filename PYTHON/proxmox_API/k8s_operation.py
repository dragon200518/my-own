
import logging
from kubernetes import client, config
import os
import sys
import re

import paramiko

config.kube_config.load_kube_config(
    config_file="/usr/lib/python2.7/dist-packages/ezs3/config_k8s")
logging.basicConfig(filename='/var/log/ezcloudstor/kube_operation.log',
                    format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S', level=logging.INFO)



SSH_USER = 'wsg'
SSH_PASSWORD = '1'
K8S_VIP = '172.17.72.64'


def ssh_do_cmd(dest_host,cmd):
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(dest_host, 22, SSH_USER, SSH_PASSWORD)
        stdin,stdout, stderr = ssh.exec_command(cmd)
        output = stdout.read().decode('utf-8')    # use utf-8
        ssh.close()
        return output
    except Exception as e:
        print('exception when ssh_do_cmd: {}, stderr is :{}'.format(str(e), stderr))
        return False


def get_k8s_active_node():
    node_list = []

    cmd="kubectl get nodes -o wide |grep -v 'NotReady' |awk '{print $6}'"
    output = ssh_do_cmd(K8S_VIP, cmd)
    try:

        if output:
            for line in output.split('\n'):
                if re.match(r"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", line):
                    node_list.append(line)

        return node_list
    except Exception as e:
        print('exception when ssh_do_cmd: {}'.format(str(e)))
        return "exception null"

def get_docker_images():
    image_list=[]

    try:
        cmd="sudo docker images |awk '{print $1}' |grep -v REPOSITORY"
        ## only get image name 

        output=ssh_do_cmd(K8S_VIP,cmd)
        if output:
            for line in output.split('\n'):
                image_list.append(line)

        return image_list
    except Exception as e:
        print('exception when ssh_do_cmd: {}'.format(str(e)))
        return "exception null"

## consider input para
def create_deployment_object(DEPLOYMENT_NAME, IMAGE_NAME, CPU, MEMORY, PVC_NAME):
    # Configureate Pod template container
    container = client.V1Container(

        name="centos-desktop",
        image=IMAGE_NAME,
        image_pull_policy="Never",
        resources=client.V1ResourceRequirements(
            requests={"cpu": CPU, "memory": MEMORY},
            limits={"cpu": CPU, "memory": MEMORY}
        ),
        liveness_probe=client.V1Probe(
            tcp_socket=client.V1TCPSocketAction(port=5901),
            initial_delay_seconds=1,
            timeout_seconds=1
        ),
        readiness_probe=client.V1Probe(
            tcp_socket=client.V1HTTPGetAction(
                path='/', port=6901, scheme='HTTP'),
            initial_delay_seconds=1,
            timeout_seconds=1
        ),
        volume_mounts=[client.V1VolumeMount(
            mount_path='/data', name='persist-data')]
    )
    # Create and configurate a spec section
    ## claim_name is pvc name, should be a input variable
    template = client.V1PodTemplateSpec(
        metadata=client.V1ObjectMeta(labels={"app": "bigtera"}),
        spec=client.V1PodSpec(containers=[container], volumes=[client.V1Volume(
            name='persist-data', persistent_volume_claim=client.V1PersistentVolumeClaimVolumeSource(claim_name=PVC_NAME))])
    )
    # Create the specification of deployment
    spec = client.V1DeploymentSpec(
        replicas=1,
        template=template,
        selector={'matchLabels': {'app': 'bigtera'}})
    # Instantiate the deployment object
    deployment = client.V1Deployment(
        api_version="apps/v1",
        kind="Deployment",
        metadata=client.V1ObjectMeta(name=DEPLOYMENT_NAME),
        spec=spec)

    return deployment


def create_deployment(api_instance, deployment):
    # Create deployement
    try:

        api_response = api_instance.create_namespaced_deployment(
            body=deployment,
            namespace="user")
        print("Deployment created. status='%s'" % str(api_response.status))
        return True
    except Exception as e:
        print("exception in create deployment. status='%s'" % str(e))


def delete_deployment(api_instance, DEPLOYMENT_NAME):
    # Delete deployment
    api_response = api_instance.delete_namespaced_deployment(
        name=DEPLOYMENT_NAME,
        namespace="user",
        body=client.V1DeleteOptions(
            propagation_policy='Foreground',
            grace_period_seconds=5))
    print("Deployment deleted. status='%s'" % str(api_response.status))


def create_persistent_volume_claim_object(PVC_NAME, CSI_NAME, PVC_SIZE):
    spec = client.V1PersistentVolumeClaimSpec(access_modes=['ReadWriteOnce'],
                                              storage_class_name=CSI_NAME,
                                              resources=client.V1ResourceRequirements(requests={"storage": PVC_SIZE}))
    pvc = client.V1PersistentVolumeClaim(
        metadata=client.V1ObjectMeta(name=PVC_NAME), spec=spec)
    return pvc

## api instance is diff from create deplyment,use CoreV1Api like:  api_core_instance = client.CoreV1Api()


def create_persistent_volume_claim(api_instance, pvc):
    # Create persistent volume claim
    try:

        api_response = api_instance.create_namespaced_persistent_volume_claim(
            body=pvc,
            namespace="user")
        print("persitent volume claim  created. status='%s'" %
              str(api_response.status))
        return True
    except Exception as e:
        print("exception in create persistent volume claim. status='%s'" % str(e))


def delete_persistent_volume_claim(api_instance, PVC_NAME):
    # Delete persistent_volume_claim
    api_response = api_instance.delete_namespaced_persistent_volume_claim(
        name=PVC_NAME,
        namespace="user",
        body=client.V1DeleteOptions(
            propagation_policy='Foreground',
            grace_period_seconds=5))
    print("persistent_volume_claim deleted. status='%s'" %
          str(api_response.status))


def create_service_object(SVC_NAME, NODE_PORT_WEB_VNC, NODE_PORT_VNC_CLIENT):

    web_vnc_port = client.V1ServicePort(
        name='web-vnc-port', protocol='TCP', port=6901, target_port=6901, node_port=NODE_PORT_WEB_VNC)
    #vnc_port=client.V1ServicePort(name='vnc-port',protocol='TCP',port=5901,target_port=5901,node_port=NODE_PORT_VNC_CLIENT)

    metadata = client.V1ObjectMeta(name=SVC_NAME)
    #spec = client.V1ServiceSpec(ports=[web_vnc_port,vnc_port],selector={"app": "bigtera"},type='NodePort')
    spec = client.V1ServiceSpec(ports=[web_vnc_port], selector={
                                "app": "bigtera"}, type='NodePort')
    service = client.V1Service(metadata=metadata, spec=spec)
    return service


def create_service(api_instance, service):
    # Create service
    try:

        api_response = api_instance.create_namespaced_service(
            body=service,
            namespace="user")
        print("service  created. status='%s'" % str(api_response.status))
        return True
    except Exception as e:
        print("exception in create service. status='%s'" % str(e))


def delete_service(api_instance, SVC_NAME):
    # Delete service
    api_response = api_instance.delete_namespaced_service(
        name=SVC_NAME,
        namespace="user",
        body=client.V1DeleteOptions(
            propagation_policy='Foreground',
            grace_period_seconds=5))
    print("service deleted. status='%s'" % str(api_response.status))


def create_namespace_object(NAMESPACE_CUSTOM):
    metadata = client.V1ObjectMeta(name=NAMESPACE_CUSTOM)
    spec = client.V1NamespaceSpec()
    namespace = client.V1Namespace(metadata=metadata, spec=spec)
    return namespace


def check_deployment_exist(DEPLOYMENT_NAME):
    deployment_list = client.AppsV1Api().list_namespaced_deployment(namespace='user').items
    for item in deployment_list:
        if item.metadata.name == DEPLOYMENT_NAME:
            return True
    return False


NAMESPACE_CUSTOM = 'user_namespace'
DEPLOYMENT_NAME = 'bigtera-deployment-2'
PVC_NAME = 'bigtera-pvc-2'
SVC_NAME = 'bigtera-svc-2'

IMAGE_NAME = 'bigtera/centos7-xfce'
CPU = '100m'
MEMORY = '500Mi'

CSI_NAME = 'csi-rbd-sc'
PVC_SIZE = '50Gi'
NODE_PORT_WEB_VNC = 30001

## temp not use the vnc client
NODE_PORT_VNC_CLIENT = 32004


def deploy_centos7_desktop(DEPLOYMENT_NUMBER, IMAGE_NAME, CPU, MEMORY, PVC_SIZE, NODE_PORT_WEB_VNC):
    
    DEPLOYMENT_NAME='bigtera-deployment-{}'.format(DEPLOYMENT_NUMBER)
    PVC_NAME='bigtera-pvc-{}'.format(DEPLOYMENT_NUMBER)
    SVC_NAME='bigtera-svc-{}'.format(DEPLOYMENT_NUMBER)

    ## deploy deployment
    deployment = create_deployment_object(
        DEPLOYMENT_NAME, IMAGE_NAME, CPU, MEMORY, PVC_NAME)
    api_instance = client.AppsV1Api()

    try:

        if create_deployment(api_instance, deployment):

            # #logging.info(
            #     "k8s debug: deployment :{} create ok".format(DEPLOYMENT_NAME))

            #deploy pvc
            pvc = create_persistent_volume_claim_object(
                PVC_NAME, CSI_NAME, PVC_SIZE)
            api_core_instance = client.CoreV1Api()
            if create_persistent_volume_claim(api_core_instance, pvc):
                # deploy svc
                svc = create_service_object(
                    SVC_NAME, NODE_PORT_WEB_VNC, NODE_PORT_VNC_CLIENT)
                if create_service(api_core_instance, svc):
                    return "ok deploy"
                else:
                    # need rollback pvc ,deployment
                    delete_persistent_volume_claim(api_core_instance, PVC_NAME)
                    delete_deployment(api_instance, DEPLOYMENT_NAME)
                    return "fail create service"
            else:
                # need rollback deployment
                delete_deployment(api_instance, DEPLOYMENT_NAME)
                return "fail create persistent volume claim"
        else:
            return "fail create deployment"

    except Exception as e:

        #roll back svc, pvc, deployment
        #if check_deployment_exist(DEPLOYMENT_NAME):
        #    delete_deployment(api_instance, DEPLOYMENT_NAME)
        print("fail deploy, exception is {}".format(str(e)))


def get_namespaced_pod_info():
    pod_state_list = []

    pod_list = client.CoreV1Api().list_namespaced_pod(namespace='user').items
    for pod in pod_list:
        #print(pod.metadata.name)
        pod_state = {}
        pod_state['name'] = pod.metadata.name

        # add pod host
        if pod.status.host_ip is not None:
            pod_state['host'] = pod.status.host_ip
        else:
            pod_state['host'] = 'null'

        # add pod volume
        for item in pod.spec.volumes:
            #print(item.persistent_volume_claim)
            if item.persistent_volume_claim is not None:
                pod_state['pvc'] = item.persistent_volume_claim.claim_name

        # add pod status
        if pod.status.container_statuses is not None:
            for item in pod.status.container_statuses:
                #print(item.state)
                if item.state.running is not None:
                    pod_state['status'] = 'runing'

        else:
            pod_state['status'] = 'null'

        pod_state_list.append(pod_state)

    return pod_state_list


def get_namespaced_pvc_info():
    pvc_state_list = []

    pvc_list = client.CoreV1Api().list_namespaced_persistent_volume_claim(
        namespace='user').items
    for item in pvc_list:
        pvc_info = {}
        #print(item.metadata.name)
        pvc_info['name'] = item.metadata.name
        pvc_info['capacity'] = item.status.capacity
        pvc_info['status'] = item.status.phase
        pvc_state_list.append(pvc_info)

    return pvc_state_list


def get_namespaced_svc_info():
    svc_state_list = []

    service_list = client.CoreV1Api().list_namespaced_service(namespace='user').items
    for item in service_list:
        svc_info = {}
        #print(item.metadata.name)
        #svc_name_list.append(item.metadata.name)
        svc_info['name'] = item.metadata.name
        if item.spec.ports is not None:
            for port in item.spec.ports:
                #print(port.node_port)
                svc_info['host_port'] = port.node_port
        svc_state_list.append(svc_info)
    return svc_state_list


def get_centos7_desktop_status():
    # return have three part
    # pod_state_list
    # created pvc_state_list
    # created svc_state_list
    deploy_status = {}

    pod_state_list = get_namespaced_pod_info()
    deploy_status['container_state'] = pod_state_list

    #add created pvc resource
    pvc_state_list = get_namespaced_pvc_info()
    deploy_status['created_pvc'] = pvc_state_list

    # add created service resource
    svc_state_list = get_namespaced_svc_info()
    deploy_status['created_svc'] = svc_state_list

    return deploy_status


def delete_centos7_desktop(DEPLOYMENT_NUMBER):

    DEPLOYMENT_NAME='bigtera-deployment-{}'.format(DEPLOYMENT_NUMBER)
    PVC_NAME='bigtera-pvc-{}'.format(DEPLOYMENT_NUMBER)
    SVC_NAME='bigtera-svc-{}'.format(DEPLOYMENT_NUMBER)
    
    print("prepare delte deploy name {}, pvc {}, svc {}".format(DEPLOYMENT_NAME,PVC_NAME,SVC_NAME))
    ## delete deployment
    api_instance = client.AppsV1Api()
    delete_deployment(api_instance, DEPLOYMENT_NAME)

    ##delete pvc
    api_core_instance = client.CoreV1Api()
    delete_persistent_volume_claim(api_core_instance, PVC_NAME)

    ##delete svc
    delete_service(api_core_instance, SVC_NAME)
