import requests
import time
import json
import paramiko
import random
from multiprocessing import Pool, Value, Lock

pve_ip = "172.17.73.248:8096"
SSH_USER = 'root'
SSH_PASSWORD = 'p@ssw0rd'

url_base_json = "https://{}/api2/json".format(pve_ip)

# ssh_do_cmd ,dest_ip is hostname, so need cluster node hostname to /etc/hosts

# for statistic

clone_retry_record=[]
config_retry_record=[]
config_ok_record=[]



######## test
vmid = 108
host_of_vm = 'node248'
new_vm_name='test-link-clone' # vm name shoud't have _, for example, test_clone not valid
new_vm_id_begin=500
target_host='node248'
new_vm_socket = 1
new_vm_cores = 1
new_vm_memory= 2048
nic='net0'
vlan_tag=24
new_vm_storage='rbd:20'
new_ip_begin='172.17.72.90'
new_netmask='255.255.252.0'
new_gateway='172.17.75.254'


# class TimerConsumer(object):
#     def __init__(self, initval=0.0):
#         self.val = Value('d', initval)
#         self.lock = Lock()



#     def increment(self, diff):
#         with self.lock:
#             old_v = self.val.value
#             self.val.value += diff 



#     def value(self):
#         with self.lock:
#             return self.val.value

# class Counter(object):
#     def __init__(self, initval=0):
#         self.val = Value('i', initval)
#         self.lock = Lock()



#     def increment(self, diff):
#         with self.lock:
#             old_v = self.val.value
#             self.val.value += diff 



#     def value(self):
#         with self.lock:
#             return self.val.value

# success_time_accu = TimerConsumer(0.0)
# success_time = Counter(0)
# fail_time = Counter(0)
# retry_time = Counter(0)

#SHARE_CONFIG_PATH='/var/share/ezfs/shareroot/share_config'
#header,session = login(pve_ip)

session = requests.Session()
def login(ip):

    try:

        # session = requests.Session()
        #url_base_extjs = "https://{}/api2/extjs".format(ip)
        url_base_json = "https://{}/api2/json".format(ip)

        parameters = {"username": "root", "password": "p@ssw0rd", "realm": "pam"}
        url_login = "{}/access/ticket".format(url_base_json)

        resp = session.post(url_login, data=parameters, verify=False)
        token = resp.json()['data']['CSRFPreventionToken']
        cookie = resp.json()['data']['ticket']
        header = {
            'Cookie': "PVEAuthCookie={}".format(cookie),
            'CSRFPreventionToken': token
        }
        session.headers.update(header)
        
    except Exception as e:
        print("exception happen login, exception is :{}\n".format(e))
    
# header,session = login(pve_ip)
def request(method, *args, **kwargs):

    try:
        resp = session.request(method, *args, **kwargs)
        #print("session header is :{}, resp :{}".format(session.headers,resp))
        # resp.raise_for_status()
        if resp.status_code == 401:
            login(pve_ip)
            resp = session.request(method, *args, **kwargs)
            #print("wsg debug ,ticket is out of date, now login again, response is :{}".format(resp))
        return resp
    except Exception as e:
        print("exception happen when request, exception is :{}\n".format(e))



def get_vm_nic_config(new_vm_id,target_host): 
    ## return nic_config like,[{'net1': 'e1000=9A:8D:95:F4:15:69,bridge=vmbr0,firewall=1'},{'net0': 'virtio=8A:D7:72:F3:AA:61,bridge=vmbr1,firewall=1,tag=25'}]
    nic_config=[]
    url_get_vm_config="{}/nodes/{}/qemu/{}/pending".format(url_base_json,target_host,new_vm_id)
    try:
        #response = session.get(url_get_vm_config, headers=header,verify=False)
        response = request('get',url_get_vm_config,verify=False)
        if response.status_code == 200:
            vm_config=response.json()['data']
            for item in vm_config:
                each_nic={}
                    #print(item['key'])
                if 'net' in item['key']:
                    #print(item['value'],item['key'])
                    each_nic[item['key']]=item['value']
                    nic_config.append(each_nic)
            return nic_config
        else:
            print("url :{},request not ok, return code is {},reason is :{}\n".format(url_get_vm_config,response.status_code,response.reason) )
            return False
    except Exception as e:
        print("exception happen when get vm nic config,exception :{}\n".format(e))

def nic_config_to_dict(config_str):
    ## return like  {'firewall': '1', 'virtio': '96:0E:F7:F4:DC:B9', 'bridge': 'vmbr0'}
    config_dict={}
    config_list=config_str.split(',')
    for each in config_list:
        #print("debug,in nic config to dict :each {}".format(each))
        #print(each.split('='))
        kv=each.split('=')
        config_dict[kv[0]]=kv[1]
    return config_dict

def nic_config_dict_to_str(config_dict):
    config_list=[]
    for k,v in config_dict.items():
        str='{}={}'.format(k,v)
        config_list.append(str)  
    #print(config_list)
    config_str=','.join(config_list)
    return config_str

def ssh_do_cmd(dest_host,cmd):
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(dest_host, 22, SSH_USER, SSH_PASSWORD)
        stdin,stdout, stderr = ssh.exec_command(cmd)
        output = stdout.read().decode('utf-8')    # use utf-8
        ssh.close()
        return output
    except Exception as e:
        print('exception when ssh_do_cmd: {},dest_host is {},exception is {}\n'.format(cmd,dest_host, str(e)))
        #return False

def check_vm_up(vmid,host_of_vm):
    try:
        cmd='qm guest exec {} hostname'.format(vmid)
        result=json.loads(ssh_do_cmd(host_of_vm,cmd))
        if result['exitcode'] == 0:
            print("vm is running")
            return True
        else:
            return False
    except Exception as e:
        print('exception when check vm up : {}\n'.format(str(e)))

def config_windows_vm_ip(vmid,host_of_vm,nic_name,new_ip,new_netmask,new_gateway):
    """
    need ssh host_of_vm  to do command qm guest exce
    host_of_vm is hostname, make sure hostname could ssh
    qm guest exec 110 netsh interface ip set address name="net0" static 172.17.73.158 255.255.252.0 172.17.75.254
    """
    cmd='qm guest exec {} netsh interface ip set address name="{}" static {} {} {}'.format(vmid,nic_name,new_ip,new_netmask,new_gateway)
    try:
        # windows boot need time,so sleep 30s
        while not check_vm_up(vmid,host_of_vm):
            time.sleep(2)
            print("vm :{} is not up, sleep 2s".format(vmid))
        result=json.loads(ssh_do_cmd(host_of_vm,cmd))
        if result['exitcode'] == 0:
            print("vm :{} ip set ok".format(vmid))
            return True
    except Exception as e:
        print("exception happen when try set windows vm ip,exception :{}\n".format(e))
    



def stop_vm_progress(taskid,target_host):
    url_stop_status="{}/nodes/{}/tasks/{}/status".format(url_base_json,target_host,taskid)

    try:
        #response = session.get(url_stop_status, headers=header,verify=False)
        response = request('get',url_stop_status,verify=False)
        if response.status_code ==200:
            return response.json()['data']['status']
        else:
            print("url :{},request not ok, return code is {},reason is :{}".format(url_stop_status,response.status_code,response.reason) )
            return False
    except Exception as e:
        print("exception happen when check vm stop progress,exception is {}".format(e))

def stop_vm(vmid,target_host):    
    url_stop_vm = "{}/nodes/{}/qemu/{}/status/stop".format(url_base_json,target_host,vmid)
    try:
        #response = session.post(url_stop_vm, headers=header,verify=False)
        response = request('post',url_stop_vm,verify=False)
        if response.status_code ==200:
            taskid=response.json()['data']
            stop_status=stop_vm_progress(taskid,target_host)
            if stop_status =='running':
                while True:
                    print("i am waiting for vm to stop")
                    time.sleep(random.randint(8,15))
                    stop_status=stop_vm_progress(taskid,target_host)
                    if stop_status== 'stopped':
                        break         
            return True  
        else:
            print("url :{},request not ok, return code is {},reason is :{}".format(url_stop_vm,response.status_code,response.reason) )
    except Exception as e:
        print("exception happen when stop vm ,exception is {}".format(e))
        

def start_vm_progress(taskid,target_host):
    url_start_status="{}/nodes/{}/tasks/{}/status".format(url_base_json,target_host,taskid)
    try:
        #response = session.get(url_start_status, headers=header,verify=False)
        response = request('get',url_start_status,verify=False)
        if response.status_code ==200:
            return response.json()['data']['status']
        else:
            print("url :{},request not ok, return code is {},reason is :{}, url text is : {}".format(url_start_status,response.status_code,response.reason,response.text) )
            return False
    except Exception as e:
        print("exception happen when check vm start progress,exception is {}".format(e))

def start_vm(new_vm_id,target_host):
    url_start_vm = "{}/nodes/{}/qemu/{}/status/start".format(url_base_json,target_host,new_vm_id)
    try:
        #header,session = login(ip)
        #response = session.post(url_start_vm, headers=header,verify=False)
        response = request('post',url_start_vm,verify=False)
        if response.status_code ==200:
            taskid=response.json()['data']
            start_status=start_vm_progress(taskid,target_host)
            if start_status =='running':
                while True:
                    print("i am waiting for vm :{} to start".format(new_vm_id))
                    time.sleep(random.randint(8,15))
                    start_status=start_vm_progress(taskid,target_host)
                    if start_status== 'stopped':
                        break
            return True
    except Exception as e:
        print("exception happen when start vm, exception is ,exception is {}".format(e))


def clone_vm_request(vmid, host_of_vm, new_vm_id, new_vm_name, target_host):
    #if clone request ok will return the clone taskid ,else return false  or 'vm already exists'
    # but i found when retry clone_vm_request, probabliy encounter vm  already exist, retry is useless,so it is a special condition

    url_clone_vm = "{}/nodes/{}/qemu/{}/clone".format(url_base_json,host_of_vm,vmid)
    post_data={
        "newid": new_vm_id,
        "name": new_vm_name,
        "target": target_host
        #"full": 1  # now need link clone,
    }
    try:
        response = request('post',url_clone_vm,data=post_data,verify=False)
        #print("response is: {}".format(response))
        #print("respons data : {},,response data type :{}".format(response.json()['data'],type(response.json()['data'])))
        return_code = response.status_code
        if return_code == 200:
            print("clone vm :{}  request return 200".format(new_vm_id))
            return response.json()['data']
        elif return_code == 500:
            # vm config file already exists
            # err msg like 'unable to create VM 102: config file already exists'
            print("clone request return code is 500，vm :{} ,return code :{}, response is :{}, reason is :{}".format(new_vm_id,response.status_code,response.json()['data'],response.reason))
            #return "vm already exists"
            if  'already exists' in response.reason:
                return 'vm already exists'
            else:
                return "need retry"
            
        else:
            #print("debug in clone vm :{} ,return code :{}, response is :{}".format(new_vm_id,response.status_code,response.json()['data']))
            print("clone vm request:{} failed , url :{} ,request not ok, return code is :{},reason is :{}\n".format(new_vm_id,url_clone_vm,return_code,response.reason) )
            return "need retry"
    except Exception as e:
        print("exception happen when do clone vm request:{},exception is {}\n".format(new_vm_id,e))
        
def clone_vm_state(taskid,host_of_vm,new_vm_id):
    url_clone_status="{}/nodes/{}/tasks/{}/status".format(url_base_json,host_of_vm,taskid)
    #header,session = login(ip)
    try:
        #print(url_clone_status)
        #response = session.get(url_clone_status, headers=header,verify=False)
        response = request('get',url_clone_status,verify=False)
        #print("clone vm :{},in progress,task id is : {}, clone task state is :{}\n".format(new_vm_id, taskid,response.json()['data']))
        return_code = response.status_code
        if return_code == 200:          
            clone_result = response.json()['data']
            if clone_result['status'] =='running':
                return "running"
            elif clone_result['status'] == 'stopped':
                if "OK" == clone_result['exitstatus']:
                    #clone sucessfull 
                    print("clone finish ok\n")
                    return "ok"
                elif "got lock request timeout" in clone_result['exitstatus']:
                    #retry_clone_flag = True
                    #print("clone timeout ,have set retry_clone_flag to true:")
                    return "timeout need retry"                         
        else:
            # other erro 
            print("vmid :{}  check clone vm state failed, url :{},request not ok, return code is {},reason is :{}\n".format(new_vm_id,url_clone_status,return_code,response.reason) )
            return "need retry"
    except Exception as e:
        print("exception happen when check  vm :{} clone progress,taskid is :{}, exception is {}\n".format(new_vm_id,taskid,e))

def clone_vm_core(vmid,host_of_vm,new_vm_id,new_vm_name,target_host):
    taskid = clone_vm_request(vmid,host_of_vm,new_vm_id,new_vm_name,target_host)
    if not taskid:
        print('taskid is :{}\n'.format(taskid))
        #need retry
        return False
    if "vm already exists" == taskid:
        # need attention
        # in condition of parallel clone request, when retry clone ,will encounter this
        return True
    else:
        clone_result = clone_vm_state(taskid,host_of_vm,new_vm_id)
        while clone_result == 'running':              
            print("vm :{} clone running, waiting for clone finish\n".format(new_vm_id))
            time.sleep(0.15)
            clone_result = clone_vm_state(taskid,host_of_vm,new_vm_id)
            if clone_result == "ok":
                return True
            elif "need retry" in clone_result:
                return False
def clone_vm(vmid,host_of_vm,new_vm_id,new_vm_name,target_host):
    CLONE_RETRY_TIMES = 5  
    while CLONE_RETRY_TIMES :
        ret = clone_vm_core(vmid,host_of_vm,new_vm_id,new_vm_name,target_host) 
        if ret :
            print("vmid :{} clone finish ok.\n".format(new_vm_id))
            return True
        print("vmid :{} retry clone, retry times :{} ".format(new_vm_id,CLONE_RETRY_TIMES))
        time.sleep(random.randint(5,10))
        CLONE_RETRY_TIMES = CLONE_RETRY_TIMES -1

    return False



def config_vm_core(new_vm_id,target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway):
    # return true if config ok , return false need retry
    #url_vm_config='https://172.17.72.45:8096/api2/extjs/nodes/hci01/qemu/300/config'
    #nic is like net0,net1
    
    target_nic_config={}
    #print("begin config vm :{} : nic {}".format(new_vm_id, nic))
    if nic and vlan_tag:
        nic_config = get_vm_nic_config(new_vm_id,target_host)
        #print(nic_config)
        if nic_config:
            for each_nic in nic_config:
                #print("debug in nic_config ,each_nic :{}".format(each_nic))
                if nic in each_nic.keys():
                    ## vlan should put in this nic
                    nic_config_dict=nic_config_to_dict(each_nic[nic])
                    #print(nic_config_dict['e1000'])

                    if 'tag' in nic_config_dict.keys():
                        #have tag before,now change
                        nic_config_dict.update({'tag':vlan_tag})
                    else:
                        #no tag before,now add
                        nic_config_dict['tag']=vlan_tag
                    target_nic_config[nic] = nic_config_dict_to_str(nic_config_dict)
                    #print('target nic config is {}'.format(target_nic_config)) 
    else:
        print('nic config info is not sufficient, so will not config nic ')

    post_data={}
    if target_nic_config is None and new_vm_storage is None:
        post_data['sockets']=new_vm_socket
        post_data['cores']=new_vm_cores
        post_data['memory']=new_vm_memory
    elif target_nic_config is None:
        post_data['sockets']=new_vm_socket
        post_data['cores']=new_vm_cores
        post_data['memory']=new_vm_memory
        post_data['scsi1']=new_vm_storage + ",discard=on"  #delete storage when delete vm
    else:
        post_data['sockets']=new_vm_socket
        post_data['cores']=new_vm_cores
        post_data['memory']=new_vm_memory
        post_data['scsi1']=new_vm_storage + ",discard=on"
        post_data.update(target_nic_config)
    url_vm_config="{}/nodes/{}/qemu/{}/config".format(url_base_json,target_host,new_vm_id)
    try:        
        #print("post data is: {}".format(post_data))
        #response = session.put(url_vm_config, headers=header,data=post_data,verify=False)
        response = request('put',url_vm_config,data=post_data,verify=False)
        if response.status_code ==200:
            return True
        else:
            # now knowned erro code is 500:cfs lock get timeout in config vm; 599: too many redirection
            print("config vm :{} failed, url :{},request not ok, return code is {},reason is :{}\n".format(new_vm_id,url_vm_config,response.status_code,response.reason) )
            return False
    except Exception as e:
        print("exception happen when config vm : {},exception is {}\n".format(new_vm_id,e))


def config_vm(new_vm_id,target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway):
    CLONE_RETRY_TIMES = 5    
    while CLONE_RETRY_TIMES :
        ret = config_vm_core(new_vm_id,target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway)
        if ret :
            print("vmid :{} config finish ok.\n".format(new_vm_id))
            return True
        print("vmid :{} retry config, retry times :{} ".format(new_vm_id,CLONE_RETRY_TIMES))
        CLONE_RETRY_TIMES = CLONE_RETRY_TIMES -1

    return False    

def clone_and_modify_vm2(vmid, host_of_vm, new_vm_id, new_vm_name, target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway):
    if clone_vm(vmid,host_of_vm,new_vm_id,new_vm_name,target_host):
        #clone finish ok,begin config vm
        if config_vm(new_vm_id,target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway):
            print("config vm :{} ok".format(new_vm_id))
            # config ok, prepare boot vm to config vm ip, now support windows 
            # if start_vm(new_vm_id,target_host):
            #     if config_windows_vm_ip(new_vm_id,target_host,nic,new_ip,new_netmask,new_gateway):
            #         return 'ok'
            #     else:
            #         print("config window ip failed")
            #         return "failed config window ip"
            # else:
            #     print("start vm failed")
            #     return "failed start vm"
        else:
            print("failed config vm".format(new_vm_id))
            return "failed config vm "

    else:
        print("failed clone vm".format(new_vm_id))
        return "failed clone vm"

def batch_clone_and_modify_vm(vmid,host_of_vm,batch_number,new_vm_id_begin,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip_begin,new_netmask,new_gateway):
    
    
    ip_list=new_ip_begin.split(".")
    ip_prefix=ip_list[0]+"."+ip_list[1]+"."+ip_list[2]+"."
    ip_last=int(ip_list[2])

    pool = Pool(20)
    for i in range(batch_number):
        new_vm_id=new_vm_id_begin+i

        ip_last=ip_last+1
        new_ip=ip_prefix+str(ip_last)
        print("begin pool.apply, new_vm_id:{},new ip:{}".format(new_vm_id,new_ip))
        pool.apply_async(clone_and_modify_vm2,(vmid, host_of_vm, new_vm_id, new_vm_name, target_host,new_vm_socket,new_vm_cores,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip,new_netmask,new_gateway))
        #time.sleep(1)
    pool.close()
    pool.join()

###################################################################################





def main():
    batch_number=120
    print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
    batch_clone_and_modify_vm(vmid,host_of_vm,batch_number,new_vm_id_begin,new_vm_memory,new_vm_storage,nic,vlan_tag,new_ip_begin,new_netmask,new_gateway)
    print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))


if __name__ == '__main__':
    main()


