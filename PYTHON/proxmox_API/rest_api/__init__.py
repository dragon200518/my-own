from os import path
import re
import json
import base64
from collections import OrderedDict
import functools
from time import time
try:
    import cPickle as pickle
except ImportError:
    import pickle

from flask import request
from flask_restplus import Api, marshal
from flask_restplus.fields import MarshallingError
from flask_restplus.representations import output_json
from werkzeug.exceptions import HTTPException
import jwt

import ezs3.kvstore as kvstore
from ezs3.log import EZLog
from ezs3.central_log import get_central_logger
from ezs3.account import AccountType
from ezs3.config import Ezs3CephConfig
from ezs3.proxmox_utils import ProxmoxAuth, btapi_port, do_check_permissions

logger = EZLog.get_logger(__name__)
central_logger = get_central_logger()


try:
    if not Ezs3CephConfig().get_cluster_name():  # avoid get mon timeout
        secret = "temperally secret"
    else:
        secret = json.loads(kvstore.get('admin_account'))["password"]
except Exception:
    secret = "temperally secret"

# TODO:
# make this config easy to change from kvstore
CONFIG = dict(enable_auth0=True,
              enable_session=True,
              expire_time=300,
              secret=secret)

api = Api(version='1.0',
          title='BIGTERA API',
          description='BigTera RestFul API',
          doc='/swagger/')

api.representations = OrderedDict([
    ('application/bigtera.vs.v1+json', output_json)
])


def check_permissions(perms, **args_cb):
    """
        args_cb is used to resolve that the API did not 
        bring the required permission parameters(likes node_name).
        args_cb: {
            '{permissions used key}': {callback_fn}
        }
        
        ex: /system/vs/Default/host/dns

        def node_name_cb(**kwargs):
            if 'host_ip' in kwargs.keys():
            return get_hostname(_host=kwargs.get('host_ip'))
        return 'localhost'


        @check_permissions(["perm", "/nodes/{node_name}", ["Sys.Audit"]], node_name=node_name_cb)

    """
    def actual_decorator(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            assert type(perms) == list, 'Type of parameter permissions should be list: {}'.format(perms)
            if 'proxmox_auth' in kwargs.keys():
                new_perms = []  # avoid arg perms will caching old value
                for ind, p in enumerate(perms):
                    new_perms.append(p)
                    if isinstance(p, str):
                        keys = re.findall("{(.*)}", p)
                        if keys and keys[0] in kwargs.keys():
                            new_perms[ind] = p.replace('{}'.format(keys[0]), kwargs.get(keys[0])).replace('{', '').replace('}','')
                        if keys and keys[0] in args_cb.keys():
                            new_perms[ind] = p.replace(
                                '{}'.format(keys[0]), 
                                args_cb.get(keys[0])(**kwargs)).replace('{', '').replace('}','')
                if not new_perms:
                    new_perms = perms
                checked, msg = do_check_permissions(kwargs['proxmox_auth']['userid_with_realm'], new_perms, _host='localhost')
                if not checked:
                    api.abort(403, msg)
            else:
                raise KeyError('Cannot get the parameter "proxmox_auth".')

            return f(*args, **kwargs)
        return wrapper
    return actual_decorator


def login_required(level=AccountType.SDS_ADMIN, get_proxmox_auth=False):
    """Decorator factroy to generate differe level login required"""
    def wrapped_login_required(f):
        @functools.wraps(f)
        def wrapper_function(*args, **kwargs):
            try:
                if str(btapi_port) in request.base_url: # FIXME: change condition for check is request from bigtera
                    pAuth = ProxmoxAuth(
                        request.method,
                        request.url_rule,
                        request.cookies,
                        request.headers.get('CSRFPreventionToken', ''))
                    # logger.info('ProxmoxAuth pass for api {}'.format(request.url_rule))

                    if get_proxmox_auth:
                        kwargs['proxmox_auth'] = pAuth.get_auth()
                    return f(*args, **kwargs)

                auth_header = request.headers.get('Authorization')
                if CONFIG['enable_auth0'] \
                        and auth_header \
                        and auth_header.startswith("Bearer "):

                    auth = jwt.decode(auth_header.split(' ')[-1],
                                      CONFIG['secret'],
                                      algorithms=['HS256'])
                    if auth.get('logged_in') and time() < auth.get('expire_time', 0) \
                            and auth.get('login_type') and auth.get('login_type') <= level:
                        return f(*args, **kwargs)
                session_id = request.cookies.get('webpy_session_id', "")
                if CONFIG['enable_session'] \
                        and session_id \
                        and path.exists('/tmp/sessions/' + session_id):
                    with open('/tmp/sessions/' + session_id) as s:
                        auth = pickle.loads(base64.decodestring(s.read()))
                        if auth.get('logged_in') and auth.get('login_type') <= level:
                            return f(*args, **kwargs)
            except HTTPException:
                raise
            except Exception as e:
                logger.exception('unexpected login failure')
                api.abort(500, str(e))
            api.abort(401, "Login Required")
        return wrapper_function
    return wrapped_login_required


def get_auth_content():
    """ return the auth info from session"""
    session_id = request.cookies.get('webpy_session_id', "")
    if session_id and path.exists('/tmp/sessions/' + session_id):
        with open('/tmp/sessions/' + session_id) as s:
            auth = pickle.loads(base64.decodestring(s.read()))
            return auth
    auth_header = request.headers.get('Authorization')
    auth = jwt.decode(auth_header.split(' ')[-1], CONFIG['secret'], algorithms=['HS256'])
    return auth


def write_audit_log(event_name, **kwargs):
    auth = get_auth_content()
    if auth:
        getattr(central_logger.auditing, event_name).log_event(
            account_type=AccountType.get_name(auth['login_type']),
            account_id=auth['login_id'], **kwargs
        )
    else:
        logger.error("Fail to get auth content")


def marshal_named_model(input_data, instance_name, fields, skip_none=False,
                        raise_if_changed=True, envelope=None, **kwargs):
    """Marshal to the form {instance_name: fields}

    @raise_if_changed: If data changed after marshalling (perhaps unnecessary fields is provided),
                       MarshallingError is raised.

    Raises: MarshallingError if validation fails.

    Example Return Value:
        {
            "rule_01": {
                "topic": "topic_callback",
                "message_queue": True,
                "enable": True,
                "verbs": ["POST", "PUT", "COPY"],
                "regex": "home/.*.jpg"
            },
            "rule_02": {
                "topic": "topic_any",
                "verbs": ["POST", "PUT", "COPY", "DELETE"],
                "enable": True,
            }
        }
    """
    rv = {}
    for name, data in input_data.iteritems():
        marshalled_data = marshal(data, fields, skip_none=skip_none, **kwargs)
        if not (isinstance(name, str) or isinstance(name, unicode)):
            raise MarshallingError(
                "Key ({}) should be string: {}".format(instance_name, repr(name))
            )

        if marshalled_data != data:
            for key in marshalled_data:
                if key in data and marshalled_data[key] != data[key]:
                    raise MarshallingError(
                        "Field '{}' is changed from '{}' to '{}' after marshal."
                        .format(key, repr(data[key]), repr(marshalled_data[key]))
                    )
            if raise_if_changed:
                raise MarshallingError(
                    "Data changed after marshal: before: {}, after: {}"
                    .format(repr(data), repr(marshalled_data))
                )
        rv[name] = marshalled_data

    return {envelope: rv} if envelope else rv
