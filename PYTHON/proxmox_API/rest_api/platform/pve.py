# -*- coding: utf-8 -*-

import requests
from flask import Blueprint, request
from flask_accept import accept
from flask_restplus import Api, Resource, fields

from ezs3 import pve_operation as pve
from ezs3.account import AccountType
from ezs3.cache import LocalCache
from ezs3.log import EZLog
from rest_api import login_required
from rest_api.platform import api

logger = EZLog.get_logger(__name__)

ns = api.namespace("bve", description="Rest api for managing pve api")

image_conf = api.model("CloneConfig", {
    "new_vm_id": fields.Integer,
    "new_vm_name": fields.String,
    "new_vm_socket": fields.Integer(description="new vm cpu socket"),
    "new_vm_cores": fields.Integer(description="new vm cpu cores"),
    "new_vm_memory": fields.Integer(description="new vm memory, in Mib"),
    "new_vm_storage": fields.String(description="The size of storage in GiB"),
    "new_vm_nic": fields.String(description="The size of storage in GiB"),
    "new_vm_nic_vlantag": fields.String(description="The size of storage in GiB"),
    "new_vm_nic_ip": fields.String(description="The size of storage in GiB"),
    "new_vm_nic_netmask": fields.String(description="The size of storage in GiB"),
    "new_vm_nic_gateway": fields.String(description="The size of storage in GiB"),
    "target_host": fields.String(description="host the new vm will be in")
})

@ns.route("/nodes/<string:host_of_vm>/qemu/<string:vmid>/cloneconfig")
class VmCloneConfig(Resource):
    @accept('application/json', 'application/bigtera.vs.v1+json')
    @ns.expect(image_conf)
    #@ns.marshal_with(image_conf)
    @login_required(level=AccountType.ADMINISTRATOR)
    def post(self,vmid,host_of_vm):
        data = api.payload
        params = {
            "vmid": vmid,
            "host_of_vm": host_of_vm,
            "new_vm_id": data.get("new_vm_id"),
            "new_vm_name": data.get("new_vm_name"),
            "new_vm_socket": data.get('new_vm_socket'),
            "new_vm_cores": data.get('new_vm_cores'),
            "new_vm_memory": data.get("new_vm_memory"),
            "new_vm_storage": data.get("new_vm_storage"),
            "nic": data.get("new_vm_nic"),
            "vlan_tag": data.get("new_vm_nic_vlantag"),
            "new_ip": data.get("new_vm_nic_ip"),
            "new_netmask": data.get("new_vm_nic_netmask"),
            "new_gateway": data.get("new_vm_nic_gateway"),
            "target_host": data.get("target_host")    
        }
        # for key,value in params.items():
        #     if value is None:
        #         if key == "new_vm_storage":
        #             continue
        #         else:
        #             return 'paramet error', 500

        try:

            result = pve.clone_and_modify_vm(**params)

            if "ok" in result:
                return "ok", 200
            else:

                return "fail", 409
        except:
            return 'exception when clone and config', 500

