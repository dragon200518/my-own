# -*- coding: utf-8 -*-

from flask import Blueprint
from flask_restplus import Api

__blueprint = Blueprint("platform_api", __name__, url_prefix="/p-api")
api = Api(
    __blueprint,
    title="Platform API",
    version="1.0",
    description="The apis to manage virtual machines and docker images",
    doc="/swagger/",
)


def create_blueprint():
    from .docker import ns as docker_ns
    from .pve import ns as pve_ns
    api.add_namespace(docker_ns)
    api.add_namespace(pve_ns)
    return __blueprint

