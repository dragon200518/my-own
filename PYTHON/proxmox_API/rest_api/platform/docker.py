# -*- coding: utf-8 -*-

import requests
from flask import Blueprint, request
from flask_accept import accept
from flask_restplus import Api, Resource, fields

from ezs3 import k8s_operation as k8s
from ezs3.account import AccountType
from ezs3.cache import LocalCache
from ezs3.log import EZLog
from rest_api import login_required
from rest_api.platform import api

logger = EZLog.get_logger(__name__)

ns = api.namespace("docker", description="Rest api for managing k8s api base on docker")

image_conf = api.model("ImageConfiguration", {
    "deployment_number": fields.Integer,
    "image_name": fields.String,
    "cpu": fields.Integer,
    "memory": fields.String(description="The size of memory in MiB"),
    "pvc_size": fields.String(description="The size of storage in GiB"),
    "svc_port": fields.Integer
})

@ns.route("/")
class DockerCreate(Resource):
    @accept('application/json', 'application/bigtera.vs.v1+json')
    @ns.expect(image_conf)
    #@ns.marshal_with(image_conf)
    @login_required(level=AccountType.ADMINISTRATOR)
    def post(self):
        data = api.payload
        params = {
            "DEPLOYMENT_NUMBER": data.get("deployment_number"),
            "IMAGE_NAME": data.get("image_name"),
            "MEMORY": data.get('memory'),
            "CPU": "{}m".format(data.get("cpu") * 1000),
            "PVC_SIZE": data.get("pvc_size"),
            "NODE_PORT_WEB_VNC": data.get("svc_port")
        }
        for i in params.values():
            if i is None:
                return 'paramet error', 500

        try:

            result = k8s.deploy_centos7_desktop(**params)
            access_info={}
            access_info['access_host']=k8s.get_k8s_active_node()
            access_info['access_port']=k8s.get_namespaced_svc_info()

            if "ok" in result:
                return access_info, 200
            elif "fail" in result:
                return result, 409
        except:
            return 'deploy counter unexpected exception, pelease delete this deploy', 500

@ns.route("/status")
class DockerStatus(Resource):
    @accept('application/json', 'application/bigtera.vs.v1+json')
    #@ns.expect(image_conf)
    #@ns.marshal_with(image_conf)
    @login_required(level=AccountType.ADMINISTRATOR)
    def get(self):
        #data = api.payload
        try:

            result = k8s.get_centos7_desktop_status()
            if result:
                return result, 200
        except:
            return ' exception', 500

@ns.route("/<string:deploy_num>")
class DockerDelete(Resource):
    @accept('application/json', 'application/bigtera.vs.v1+json')
    #@ns.expect(image_conf)
    #@ns.marshal_with(image_conf)
    @login_required(level=AccountType.ADMINISTRATOR)
    def delete(self,deploy_num):
        k8s.delete_centos7_desktop(deploy_num)
        return "delete ok", 200

@ns.route("/images")
class DockerImages(Resource):
    @accept('application/json', 'application/bigtera.vs.v1+json')
    #@ns.expect(image_conf)
    #@ns.marshal_with(image_conf)
    @login_required(level=AccountType.ADMINISTRATOR)
    def get(self):
        #data = api.payload
        try:

            result = k8s.get_docker_images()
            if result:
                resp={}
                resp['image_name']=result
                return resp, 200
        except:
            return ' exception', 500

