from boto3.session import Session
import boto3
#Client初始化
access_key = "07XYFLT9921WDPPWWN9T"
secret_key = "weCjR5lZlCteXHVnID2HOlXxiDRPXRQBvQpliUiU"

url = "http://172.17.73.21:80"

session = Session(access_key, secret_key)
s3_client = session.client('s3', endpoint_url=url)
#Client初始化结束
#列出该用户拥有的桶
#for bucket in s3_client.list_buckets()['Buckets']:
#    print(bucket['Name'])


print([bucket['Name'] for bucket in s3_client.list_buckets()['Buckets']])


def upload_file(s3_client,file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param s3_client: s3 connection
    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    try:
        response = s3_client.upload_file(file_name, bucket, object_name,ExtraArgs={'Metadata': {'mykey': 'myvalue'}})
    except ClientError as e:
        logging.error(e)
        return False
    return True



def tagging_object(s3_client,bucket,file_name):
    """tag a object name (same as the uploaded file) in S3 bucket

    :param s3_client: s3 connection
    :param bucket: Bucket to upload to
    :param file_name: S3 object name. commonly as the filename that uploaded to the bucket 
    """

    response = s3_client.put_object_tagging(
        Bucket=bucket,
        Key=file_name,
        Tagging={
            'TagSet': [
                {
                    'Key': 'Key3',
                    'Value': 'Value3',
                },
                {
                    'Key': 'Key4',
                    'Value': 'Value4',
                },
            ],
        },
)



def tagging_object_with_tag(s3_client,bucket,file_name,tag):
    """tag a object name (same as the uploaded file) in S3 bucket

    :param s3_client: s3 connection
    :param bucket: Bucket to upload to
    :param file_name: S3 object name. commonly as the filename that uploaded to the bucket 
    :parm tag: example {'Key':'key1','Value': 'Value3'}
    """

    response = s3_client.put_object_tagging(
        Bucket=bucket,
        Key=file_name,
        Tagging={
            'TagSet': [
                {
                    'Key': tag['Key'],
                    'Value': tag['Value'],
                }
            ],
        },
)



dest_bucket='wsg'
file_name='check_dpkg_new_version.py'
metadata_dict={'key1':'wsgkey','key2':'value2'}


upload_file(s3_client,file_name,dest_bucket)
#tagging_object(s3_client,dest_bucket,file_name)


tag_dict={'Key': 'wsg-tag-key','Value': '国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国国'}
tagging_object_with_tag(s3_client,dest_bucket,file_name,tag_dict)





