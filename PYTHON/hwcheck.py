#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import json
import logging
import os
import re
import tempfile
import subprocess
import time
import threading
import Queue
import ctypes
import ctypes.util
import fcntl
import os
import platform
from multiprocessing.pool import Pool
logging.basicConfig()
logger = logging.getLogger()

RED = "\033[31m"
GREEN = "\033[32m"
NO_COLOR = "\033[0m"

MIN_HEALTH_PERCENT = 25


class DiskType:
    NVME_SSD = 0
    SATA_SSD = 1
    SAS_HDD = 2
    SATA_HDD = 3
    SATA_DOM = 4
    VIRTUAL_DISK = 5


class Errors:
    # For HDD
    Smart5Error = "Disk has reallocated sectors({})"
    Smart187Error = "Disk has uncorrectable errors({})"
    Smart197Error = "Disk has unstable sectors({})"
    Smart198Error = "Disk has uncorrectable sectors({})"
    SmartInfoError = 'Disk has no smart info'
    # For SATA DOM
    SataDomError = "This disk is a sata dom."
    DiskWearOut = "More than {}% of disk has worn out,and the remaining {}%"
    # For NVME
    NvmeMediaError = "Disk has media error({})"
    # For SAS
    SAS_GROWN_DEFECT = "elements in grown defect list({})"


def yes_no_bool(data):
    return data.lower() == 'yes'


class NoVdExists(Exception):
    pass


RAID_CLI = '/usr/local/bin/storcli64'
PROGRESS_REGEX = re.compile(r'Completed (\d+%) in')


class DoCommandError(RuntimeError):
    def __init__(self, stderr, errno=0, stdout='', cmd=""):
        RuntimeError.__init__(self, stderr)
        self.errno, self.stdout, self.stderr = errno, stdout, stderr
        self.do_cmd = cmd

    def __str__(self):
        return "DoCommandError: errno {} stdout '{}' stderr '{}' cmd '{}'" \
               .format(self.errno, self.stdout, self.stderr, self.cmd)


class DoCommandTimedOut(RuntimeError):
    pass


def do_cmd(cmd, timeout=0, force=False):
   # logger.debug(u"command '{}' (timeout={}, force={})"
   #              .format(cmd, timeout, force))

    cmdstr = cmd.encode('utf-8')
    if timeout <= 0:
        with tempfile.TemporaryFile('w+') as outfp:
            with tempfile.TemporaryFile('w+') as errfp:
                p = subprocess.Popen([cmdstr],
                                     stdout=outfp,
                                     stderr=errfp,
                                     shell=True,
                                     close_fds=True)
                p.wait()
                outfp.flush()   # don't know if this is needed
                outfp.seek(0)
                output = outfp.read()
                errfp.flush()   # don't know if this is needed
                errfp.seek(0)
                err = errfp.read()
    else:
        with tempfile.TemporaryFile('w+') as outfp:
            with tempfile.TemporaryFile('w+') as errfp:
                p = subprocess.Popen([cmdstr],
                                     stdout=outfp,
                                     stderr=errfp,
                                     shell=True,
                                     close_fds=True)
                wait_loop = 0
                while p.poll() is None:
                    if wait_loop < 10:
                        t = min(timeout, 0.002)
                    else:
                        t = min(timeout, 0.005)
                    wait_loop += 1
                    time.sleep(t)
                    timeout -= t
                    if timeout <= 0:
                       # logger.debug("killing process %d and its child processes", p.pid)
                        try:
                            proc = psutil.Process(p.pid)
                            for c in proc.children(recursive=True):
                                c.kill()
                            proc.kill()
                        except psutil.NoSuchProcess:
                            pass

                        if force:
                           # logger.warning(u"command '{}' timeout".format(cmd))
                            return ""
                        else:
                            raise DoCommandTimedOut(
                                u"command '{}' timeout".format(cmd)
                            )

                outfp.flush()   # don't know if this is needed
                outfp.seek(0)
                output = outfp.read()
                errfp.flush()   # don't know if this is needed
                errfp.seek(0)
                err = errfp.read()

    # prevent UnicodeDecodeError if invalid char in error/output
    err_str = unicode(err, 'utf-8', 'ignore')
    out_str = unicode(output, 'utf-8', 'ignore')
    if p.returncode != 0:
        if force:
           # logger.warning(u"command '{}' failed: {} ({})"
           #                .format(cmd, err_str, p.returncode))
            return ""
        else:
            raise DoCommandError(err, p.returncode, output, cmdstr)

    # logger.debug(u"command '{}' returns '{}'".format(cmd, out_str))

    return output


def reformat(key, value, rule):
    if isinstance(value, list):
        new_value = []
        for each_value in value:
            current_value = {}
            for k, r in rule:
                new_k, new_v = reformat(k, each_value.get(k), r)
                current_value[new_k] = new_v
            new_value.append(current_value)
        return key, new_value
    elif isinstance(value, dict):
        # handle bbu's parsed data
        new_value = {}
        for k, r in rule:
            if '_name' == k:
                key = r
            else:
                new_k, new_v = reformat(k, value.get(k), r)
                new_value[new_k] = new_v

        key = key.lower().replace(' ', '_')
        return key, new_value
    else:
        new_key, parser, params = rule

        if new_key is None:
            new_key = key.lower().replace(' ', '_')
        if parser is None:
            parser = do_nothing

        if value is None:       # e.g. Consistency Check
            return new_key, None
        else:
            return new_key, parser(value, *params)


def parse_line(line, dividers=(':',)):
    key = None
    value = None

    for divider in dividers:
        try:
            key, value = line.split(divider, 1)
            break
        except ValueError:
            pass

    if key is None or value is None:
        raise ValueError

    return key.strip(), value.strip()


def do_nothing(data, *params):
    return data


def reg_parse(data, reg):
    result = re.match(reg, data)
    if result:
        return result.group(1)
    else:
        return None


def transfer_unit(data, reg=None):
    if reg:
        data = reg_parse(data, reg)
    if 'i' in data:
        return data
    str_data = list(data)

    str_data.insert(-1, 'i')
    return "".join(str_data)


def yes_no_bool(data):
    return data.lower() == 'yes'


def inquiry_data_parse(data):
    data = data.strip()
    new_data = ''
    space_cnt = 0
    for each_char in data:
        if each_char == ' ':
            if space_cnt == 0:
                space_cnt += 1
                new_data += each_char
        else:
            space_cnt = 0
            new_data += each_char

    return new_data


def only_upper_case(string):
    return ''.join([char for char in string if char.isupper()])


class MegacliParser(object):
    BASE_CMD = RAID_CLI + ' {} -NoLog'
    FORMAT_RULES = {}

    def __init__(self, cmd_options):
        self.cmd_str = self.BASE_CMD.format(cmd_options)
        self.lines = None
        self.raw_data = None
        self.parsed_data = None
        self.formatted_data = None

        self.get_raw_data()
        self.parse_data()

    def get_raw_data(self):
        self.raw_data = do_cmd(self.cmd_str, timeout=10, force=True)
        self.lines = [l.strip()
                      for l in self.raw_data.splitlines() if l.strip()]

    def parse_data(self):
        raise NotImplementedError

    def get_formatted_data(self, format_rules=None):
        if format_rules is None:
            format_rules = self.FORMAT_RULES

        formatted_data = []
        for data in self.parsed_data:
            datum = {}
            for key, rule in format_rules:
                new_key, new_value = reformat(key, data.get(key), rule)
                datum[new_key] = new_value

            formatted_data.append(datum)

        self.formatted_data = formatted_data
        return formatted_data


def parse_line(line, dividers=(':',)):
    key = None
    value = None

    for divider in dividers:
        try:
            key, value = line.split(divider, 1)
            break
        except ValueError:
            pass

    if key is None or value is None:
        raise ValueError

    return key.strip(), value.strip()


class LdPdParser(MegacliParser):
    FORMAT_RULES = [
        ('Adapter', ('adapter', None, ())),
        ('pds', [
            ('Drive has flagged a S.M.A.R.T alert',
             ('smart_error', yes_no_bool, ())),
        ]),
    ]

    def __init__(self, options='-LdPdInfo -aALL'):
        super(LdPdParser, self).__init__(options)

    def parse_data(self):
        blocks = []
        current_block = {}
        lines = self.raw_data.splitlines()

        for line in lines:
            if line.strip():
                key, value = parse_line(line, (':', '#'))

                if key in ('Adapter', 'PD', 'Virtual Drive'):
                    if current_block:
                        blocks.append(current_block)
                        current_block = {}
                    current_block[key] = value
                elif key == 'Exit Code':
                    if current_block:
                        blocks.append(current_block)
                    break
                else:
                    current_block[key] = value

        adapter = None
        virtual_driver = {}
        self.parsed_data = []
        for block in blocks:
            if 'Adapter' in block:
                if virtual_driver:
                    self.parsed_data.append(virtual_driver)
                    virtual_driver = {}
                adapter = block['Adapter']
            elif 'Virtual Drive' in block:
                if virtual_driver:
                    self.parsed_data.append(virtual_driver)
                virtual_driver = block
                virtual_driver['Adapter'] = adapter
                virtual_driver['pds'] = []
            elif 'PD' in block:
                virtual_driver['pds'].append(block)
            else:
                self.parsed_data.append(block)

        if virtual_driver:
            self.parsed_data.append(virtual_driver)


def get_ldpd_info(format_rules=None, details=None):
    return BaseRaid().get_ldpd_info(format_rules, details)


def get_unassigned_pds(format_rules=None, details=None):
    return BaseRaid().get_unassigned_pds(format_rules, details)


RAID_TYPE_CONF_PATH = "/var/run/"
RAID_TYPE_CONF = "raid_type.conf"


class BaseRaid(object):
    def __init__(self):
        self.raid_type = None

    def load(self):
        raid_type_conf = os.path.join(RAID_TYPE_CONF_PATH, RAID_TYPE_CONF)

        try:
            if not os.path.exists(raid_type_conf):
                self.save()

            if os.path.isfile(raid_type_conf):
                with open(raid_type_conf) as f:
                    self.raid_type = json.loads(f.read())

        except Exception as e:
            # logger.exception("Fail to load raid type info")
            print("Fail to load raid type info")

    def save(self):
        raid_type_conf = os.path.join(RAID_TYPE_CONF_PATH, RAID_TYPE_CONF)
        try:
            output = do_cmd(
                '/usr/local/bin/arcconf list | grep -E "Controllers found"', timeout=10)
            if int(output.strip().split(':')[1]) > 0:
                raw_ni = json.dumps('arcconf')
                with open(raid_type_conf, 'w') as f:
                    f.write(raw_ni)
            else:
                raw_ni = json.dumps('megacli')
                with open(raid_type_conf, 'w') as f:
                    f.write(raw_ni)

        except Exception as e:
            # logger.debug("arcconf command line error,so we use megacli64")
            raw_ni = json.dumps('megacli')
            with open(raid_type_conf, 'w') as f:
                f.write(raw_ni)

    def get_ldpd_info(self, format_rules=None, details=None):
        self.load()
        if self.raid_type == 'arcconf':
            raise NotImplementedError
        elif self.raid_type == 'megacli':
            return LSI_RAID().get_ldpd_info(format_rules, details)

    def get_unassigned_pds(self, format_rules=None, details=None):
        self.load()
        if self.raid_type == 'arcconf':
            raise NotImplementedError
        elif self.raid_type == 'megacli':
            return LSI_RAID().get_unassigned_pds(format_rules, details)


class AdapterPciInfoParser(MegacliParser):
    FORMAT_RULES = [
        ('Adapter', (None, None, ())),
        ('Bus Number', ('bus', None, ())),
        ('Device Number', ('device', None, ())),
        ('Function Number', ('function', None, ())),
    ]

    def __init__(self, options='-AdpGetPciInfo -aALL'):
        super(AdapterPciInfoParser, self).__init__(options)

    def parse_data(self):
        blocks = []
        current_block = {}
        for line in self.lines:
            if line.startswith('PCI information for Controller'):
                if current_block:
                    blocks.append(current_block)
                    current_block = {}
                current_block['Adapter'] = line.rsplit(' ', 1)[1]
            elif '------' in line:  # ignore
                continue
            else:
                key, value = parse_line(line, (':',))
                if key == 'Exit Code':
                    blocks.append(current_block)
                    current_block = {}
                else:
                    current_block[key] = value

        self.parsed_data = blocks


def target_id_to_disk(target_id, adapter='0', pci_info=None):
    if not pci_info:
        pci_info = AdapterPciInfoParser().get_formatted_data()

    bus_no = None
    device_no = None
    function_no = None
    for info in pci_info:
        if info['adapter'] == str(adapter):
            bus_no = info['bus']
            device_no = info['device']
            function_no = info['function']
            break

    if not all((bus_no, device_no, function_no)):
        print(
            "Failed to get Adapter's Bus Number, Device Number or Function Number for Adapter {}, PCI info: {}".format(
                adapter, pci_info
            )
        )
        return None

    slot_name = "{:>02}:{:>02}.{}".format(bus_no, device_no, function_no)
    pattern = re.compile(
        r':{}-scsi-\d+:\d+:{}:\d+$'.format(slot_name, target_id))
    disk_path = None
    dev_path = '/dev/disk/by-path'
    for disk in os.listdir(dev_path):
        if pattern.search(disk):
            disk_path = os.path.realpath(os.path.join(dev_path, disk))
            break

    if not disk_path:
        print('Failed to find corresponding sdX for target id [{}]'
                     .format(target_id))
    return disk_path


SSD_INDICATOR = {
    'INTEL': '233',
    'INDILINX': '209',
    'MICRON': '202',
    'SAMSUNG': '177',
    'SANDFORCE': '231',
    'SANDISK': '178'
}


def get_ssd_health(adapter, vd_num, pd, pci_info=None):
    if pd['media_type'] != 'SSD':
        return ('none', -1)

    ssd_brand = None
    for brand in SSD_INDICATOR:
        if brand in pd['inquiry_data'].upper():
            ssd_brand = brand
            break

    healthy, lifetime_remaining = is_smart_warn(
        pd['device_id'], vd_num, ssd_brand, adapter, pci_info=pci_info)
    return (healthy, lifetime_remaining)


def is_smart_warn(device_id, vd, ssd_brand=None, adapter='0', pci_info=None):
    disk = target_id_to_disk(vd, adapter, pci_info=pci_info)
    if disk is None:
        print(
            'Failed to get disk path of vd [{}], adapter [{}]'.format(vd, adapter))

    if ssd_brand:
        attr = '^{}'.format(SSD_INDICATOR[ssd_brand])
        try:
            output = do_cmd('smartctl -a -d megaraid,{} {}|grep {}'.format(device_id, disk, attr),
                            timeout=10)
        except DoCommandError:
            return DiskHealthState.get_name(DiskHealthState.UNKNOWN), -1
    else:
        try:
            attr = None
            output = None
            try:
                raw = do_cmd('smartctl -a -d megaraid,{} {}'.format(device_id, disk),
                             timeout=10)
            except DoCommandError as e:
                logger.info(
                    "smartctl -a -d megaraid，{} {} return error e.errno {}".format(device_id, disk, e.errno))
                raw = e.stdout + '\n' + e.stderr

            raw_lines = raw.split('\n')
            for line in raw_lines:
                if line.startswith('Model Family:'):
                    model_name = line.split(":")[1].upper()
                    for brand in SSD_INDICATOR:
                        if brand in model_name:
                            logger.debug("BRAND is {}".format(brand))
                            attr = '{}'.format(SSD_INDICATOR[brand])
                            break
                    else:
                        break
            if not attr:
                attr = "{}".format(SSD_INDICATOR['INTEL'])
                logger.warning(
                    'SSD brand unknown, use Media_Wearout_Indicator to filter SSD health')

            for line in raw_lines:
                if line.startswith(attr):
                    output = line
                    break
            if not output:
                return DiskHealthState.get_name(DiskHealthState.UNKNOWN), -1
        except Exception as e:
            logger.exception("error happened when try to get ssd duration")
            raise e

    cols = output.split()
    value = int(cols[3])

    if value <= 25:
        return DiskHealthState.get_name(DiskHealthState.ERROR), value
    elif value <= 15:
        return DiskHealthState.get_name(DiskHealthState.WARNING), value
    else:
        return DiskHealthState.get_name(DiskHealthState.OK), value


class DiskHealthState:
    [
        UNKNOWN,
        OK,
        WARNING,
        ERROR
    ] = range(4)

    @classmethod
    def get_name(cls, value):
        for k, v in cls.__dict__.iteritems():
            if v == value:
                return k
        return cls.get_name(cls.UNKNOWN)


class LdpdDetailType:
    VD_DISK_NAME = 0b0001
    SSD_HEALTH = 0b0010
    REBUILD_PROGRESS = 0b0100
    DISK_EVENTS = 0b1000


def get_ldpd_details(flag, target_id, adapter, vd_num, pd, pci_info=None):
    ret = {
        'disk': None,
        'healthy': None,
        'lifetime_remaining': None,
        'rebuild_progress': None,
        'disk_events': None
    }
    if flag & LdpdDetailType.VD_DISK_NAME:
        ret['disk'] = target_id_to_disk(target_id, adapter, pci_info=pci_info)
    if flag & LdpdDetailType.SSD_HEALTH:
        ret['healthy'] =\
            get_ssd_health(adapter, vd_num, pd, pci_info=pci_info)
    return ret


class NoVdExists(Exception):
    # to get smart info of a physical disk of a raid card
    # `smartctl` command requires at least 1 vd's disk path
    pass


class PdParser(MegacliParser):
    FORMAT_RULES = [
        ('Adapter', (None, None, ())),
        ('Enclosure Device ID', ('enclosure', None, ())),
        ('Slot Number', ('slot', None, ())),
        ('WWN', (None, None, ())),
        ('Inquiry Data', (None, inquiry_data_parse, ())),
        ('Media Error Count', ('media_error', int, ())),
        ('Other Error Count', ('other_error', int, ())),
        ('Drive has flagged a S.M.A.R.T alert', ('smart_error', yes_no_bool, ())),
        ('Type', (None, reg_parse, (r'^(\w+)',))),
        ('Array #', ('array', None, ())),
        ('Firmware state', (None, None, ())),
        ('Media Type', (None, only_upper_case, ())),
        ('PD Type', (None, None, ())),
        ('Raw Size', ('size', transfer_unit, (r'^([0-9.]+\s*\w+)',))),
        ('Device Id', (None, None, ())),
        # ("Drive's postion", ('drive_position', None, ())),
    ]

    def __init__(self, options='-PDList -aALL'):
        super(PdParser, self).__init__(options)
        if 'storcli' in RAID_CLI:
            self.FORMAT_RULES.append(
                ("Drive's postion", ('drive_position', None, ())))
        else:
            self.FORMAT_RULES.append(
                ("Drive's position", ('drive_position', None, ())))

    def parse_data(self):
        blocks = []
        current_block = {}
        adapter = None
        for line in self.lines:
            key, value = parse_line(line, (':', '#'))

            if key == 'Adapter':
                adapter = value
            elif key == 'Enclosure Device ID':
                if current_block:
                    blocks.append(current_block)
                current_block = {'Adapter': adapter, key: value}
            elif key == 'Exit Code':
                if current_block:
                    blocks.append(current_block)
            else:
                current_block[key] = value

        self.parsed_data = blocks


class LdParser(MegacliParser):
    FORMAT_RULES = [
        ('Adapter', (None, None, ())),
        ('Virtual Drive', ('target_id', reg_parse, (r'.*Target Id: (\d+)',))),
    ]

    def __init__(self, options='-LdInfo -lALL -aALL'):
        super(LdParser, self).__init__(options)

    def parse_data(self):
        self.parsed_data = []
        adapter = None
        ldinfo = None
        for line in self.lines:
            if line.startswith('Adapter'):
                adapter = line.split()[1]
            else:
                key, value = parse_line(line)
                if key == 'Virtual Drive':
                    if ldinfo:
                        self.parsed_data.append(ldinfo)
                    ldinfo = {
                        'Adapter': adapter,
                        'Virtual Drive': value
                    }
                    continue

                if key == 'Exit Code':
                    if ldinfo:
                        self.parsed_data.append(ldinfo)
                    break

                ldinfo[key] = value


class LdPdParser(MegacliParser):
    FORMAT_RULES = [
        ('Adapter', ('adapter', None, ())),
        ('Virtual Drive', ('vd', reg_parse, (r'^(\d+)',))),
        ('Virtual Drive', ('target_id', reg_parse, (r'.*Target Id: (\d+)',))),
        ('RAID Level', ('raid_type', reg_parse, (r'^Primary-(\d+)',))),
        ('Size', (None, transfer_unit, ())),
        ('State', (None, None, ())),
        ('Current Cache Policy', (None, None, ())),
        ('Disk Cache Policy', (None, None, ())),
        ('Number Of Drives', ('drive_num', None, ())),
        ('Strip Size', ('strip_size', None, ())),
        ('Check Consistency', ('consistency_check', None, ())),
        ('Background Initialization', ('background_init', None, ())),
        ('pds', [
            ('PD', ('pd', reg_parse, (r'^(\d+)',))),
            ('Enclosure Device ID', ('enclosure', None, ())),
            ('Slot Number', ('slot', None, ())),
            ('Media Error Count', ('media_error', int, ())),
            ('Other Error Count', ('other_error', int, ())),
            ('Drive has flagged a S.M.A.R.T alert',
             ('smart_error', yes_no_bool, ())),
            ('Firmware state', (None, None, ())),
            ('Media Type', (None, only_upper_case, ())),
            ('PD Type', (None, None, ())),
            ('Device Id', (None, None, ())),
            ('Raw Size', ('size', transfer_unit, (r'^([0-9.]+\s*\w+)',))),
            ('Inquiry Data', (None, inquiry_data_parse, ())),
        ]),
    ]

    def __init__(self, options='-LdPdInfo -aALL'):
        super(LdPdParser, self).__init__(options)

    def parse_data(self):
        blocks = []
        current_block = {}
        lines = self.raw_data.splitlines()

        for line in lines:
            if line.strip():
                key, value = parse_line(line, (':', '#'))

                if key in ('Adapter', 'PD', 'Virtual Drive'):
                    if current_block:
                        blocks.append(current_block)
                        current_block = {}
                    current_block[key] = value
                elif key == 'Exit Code':
                    if current_block:
                        blocks.append(current_block)
                    break
                else:
                    current_block[key] = value

        adapter = None
        virtual_driver = {}
        self.parsed_data = []
        for block in blocks:
            if 'Adapter' in block:
                if virtual_driver:
                    self.parsed_data.append(virtual_driver)
                    virtual_driver = {}
                adapter = block['Adapter']
            elif 'Virtual Drive' in block:
                if virtual_driver:
                    self.parsed_data.append(virtual_driver)
                virtual_driver = block
                virtual_driver['Adapter'] = adapter
                virtual_driver['pds'] = []
            elif 'PD' in block:
                virtual_driver['pds'].append(block)
            else:
                self.parsed_data.append(block)

        if virtual_driver:
            self.parsed_data.append(virtual_driver)

# NVME

class NVMEPassthruCommand(ctypes.Structure):
    _fields_ = [
        ('opcode', ctypes.c_ubyte),
        ('flags', ctypes.c_ubyte),
        ('rsvd1', ctypes.c_uint16),
        ('nsid', ctypes.c_uint32),
        ('cdw2', ctypes.c_uint32),
        ('cdw3', ctypes.c_uint32),
        ('metadata', ctypes.c_uint64),
        ('addr', ctypes.c_uint64),
        ('metadata_len', ctypes.c_uint32),
        ('data_len', ctypes.c_uint32),
        ('cdw10', ctypes.c_uint32),
        ('cdw11', ctypes.c_uint32),
        ('cdw12', ctypes.c_uint32),
        ('cdw13', ctypes.c_uint32),
        ('cdw14', ctypes.c_uint32),
        ('cdw15', ctypes.c_uint32),
        ('timeout_ms', ctypes.c_uint32),
        ('result', ctypes.c_uint32)
    ]


class NVMESmartLog(ctypes.Structure):
    _fields_ = [
        ('critical_warning', ctypes.c_ubyte),
        ('temperature', ctypes.c_ubyte * 2),
        ('avail_spare', ctypes.c_ubyte),
        ('spare_thresh', ctypes.c_ubyte),
        ('percent_used', ctypes.c_ubyte),
        ('rsvd6', ctypes.c_ubyte * 26),
        ('data_units_read', ctypes.c_ubyte * 16),
        ('data_units_written', ctypes.c_ubyte * 16),
        ('host_reads', ctypes.c_ubyte * 16),
        ('host_writes', ctypes.c_ubyte * 16),
        ('ctrl_busy_time', ctypes.c_ubyte * 16),
        ('power_cycles', ctypes.c_ubyte * 16),
        ('power_on_hours', ctypes.c_ubyte * 16),
        ('unsafe_shutdowns', ctypes.c_ubyte * 16),
        ('media_errors', ctypes.c_ubyte * 16),
        ('num_err_log_entries', ctypes.c_ubyte * 16),
        ('warning_temp_time', ctypes.c_uint32),
        ('critical_comp_time', ctypes.c_uint32),
        ('temp_sensor', ctypes.c_uint16 * 8),
        ('thm_temp1_trans_count', ctypes.c_uint32),
        ('thm_temp2_trans_count', ctypes.c_uint32),
        ('thm_temp1_total_time', ctypes.c_uint32),
        ('thm_temp2_total_time', ctypes.c_uint32),
        ('rsvd232', ctypes.c_ubyte * 280)
    ]

class NVMEIdPowerState(ctypes.Structure):
    _fields_ = [
        ('max_power', ctypes.c_uint16),
        ('rsvd2', ctypes.c_ubyte),
        ('flags', ctypes.c_ubyte),
        ('entry_lat', ctypes.c_uint32),
        ('exit_lat', ctypes.c_uint32),
        ('read_tput', ctypes.c_ubyte),
        ('read_lat', ctypes.c_ubyte),
        ('write_tput', ctypes.c_ubyte),
        ('write_lat', ctypes.c_ubyte),
        ('idle_power', ctypes.c_uint16),
        ('idle_scale', ctypes.c_ubyte),
        ('rsvd19', ctypes.c_ubyte),
        ('active_power', ctypes.c_uint16),
        ('active_work_scale', ctypes.c_ubyte),
        ('rsvd23', ctypes.c_ubyte * 9)
    ]


class NVMEIdCtrl(ctypes.Structure):
    _fields_ = [
        ('vid', ctypes.c_uint16),
        ('ssvid', ctypes.c_uint16),
        ('sn', ctypes.c_char * 20),
        ('mn', ctypes.c_char * 40),
        ('fr', ctypes.c_char * 8),
        ('rab', ctypes.c_ubyte),
        ('ieee', ctypes.c_ubyte * 3),
        ('cmic', ctypes.c_ubyte),
        ('mdts', ctypes.c_ubyte),
        ('cntlid', ctypes.c_uint16),
        ('ver', ctypes.c_uint32),
        ('rtd3r', ctypes.c_uint32),
        ('rtd3e', ctypes.c_uint32),
        ('oaes', ctypes.c_uint32),
        ('rsvd96', ctypes.c_ubyte * 160),
        ('oacs', ctypes.c_uint16),
        ('acl', ctypes.c_ubyte),
        ('aerl', ctypes.c_ubyte),
        ('frmw', ctypes.c_ubyte),
        ('lpa', ctypes.c_ubyte),
        ('elpe', ctypes.c_ubyte),
        ('npss', ctypes.c_ubyte),
        ('avscc', ctypes.c_ubyte),
        ('apsta', ctypes.c_ubyte),
        ('wctemp', ctypes.c_uint16),
        ('cctemp', ctypes.c_uint16),
        ('mtfa', ctypes.c_uint16),
        ('hmpre', ctypes.c_uint32),
        ('hmmin', ctypes.c_uint32),
        ('tnvmcap', ctypes.c_ubyte * 16),
        ('unvmcap', ctypes.c_ubyte * 16),
        ('rpmbs', ctypes.c_uint32),
        ('rsvd316', ctypes.c_ubyte * 196),
        ('sqes', ctypes.c_ubyte),
        ('cqes', ctypes.c_ubyte),
        ('rsvd514', ctypes.c_ubyte * 2),
        ('nn', ctypes.c_uint32),
        ('oncs', ctypes.c_uint16),
        ('fuses', ctypes.c_uint16),
        ('fna', ctypes.c_ubyte),
        ('vwc', ctypes.c_ubyte),
        ('awun', ctypes.c_uint16),
        ('awupf', ctypes.c_uint16),
        ('nvscc', ctypes.c_ubyte),
        ('rsvd531', ctypes.c_ubyte),
        ('acwu', ctypes.c_uint16),
        ('rsvd534', ctypes.c_ubyte * 2),
        ('sgls', ctypes.c_uint32),
        ('rsvd540', ctypes.c_ubyte * 1508),
        ('psd', NVMEIdPowerState * 32),
        ('vs', ctypes.c_ubyte * 1024)
    ]

def int128_to_double(data):
    result = 0

    for byte in reversed(data):
        result *= 256
        result += byte

    return result


class IoctlGeneric(object):
    _IOC_NRBITS = 8
    _IOC_TYPEBITS = 8
    _IOC_SIZEBITS = 14
    _IOC_DIRBITS = 2
    _IOC_NONE = 0
    _IOC_WRITE = 1
    _IOC_READ = 2

    @classmethod
    def ioc(cls, direction, request_type, request_nr, size):
        _IOC_NRSHIFT = 0
        _IOC_TYPESHIFT = _IOC_NRSHIFT + cls._IOC_NRBITS
        _IOC_SIZESHIFT = _IOC_TYPESHIFT + cls._IOC_TYPEBITS
        _IOC_DIRSHIFT = _IOC_SIZESHIFT + cls._IOC_SIZEBITS
        return (
            (direction << _IOC_DIRSHIFT) |
            (request_type << _IOC_TYPESHIFT) |
            (request_nr << _IOC_NRSHIFT) |
            (size << _IOC_SIZESHIFT)
        )

def IOWR(request_type, request_nr, size):
    calc = IoctlGeneric()
    return calc.ioc(calc._IOC_READ | calc._IOC_WRITE, ord(request_type), request_nr, size)


NVME_ADMIN_IDENTIFY = 0x06
NVME_LOG_SMART = 0x02
NVME_ADMIN_GET_LOG_PAGE = 0x02
NSID = 0xffffffff
NSID_ZERO = 0x00000000

_ioctl_fn = None


def _get_ioctl_fn():
    global _ioctl_fn
    if _ioctl_fn is not None:
        return _ioctl_fn
    libc_name = ctypes.util.find_library('c')
    if not libc_name:
        raise Exception('Unable to find c library')
    libc = ctypes.CDLL(libc_name, use_errno=True)
    _ioctl_fn = libc.ioctl
    return _ioctl_fn


def ioctl(fd, request, *args):
    ioctl_args = [ctypes.c_int(fd), ctypes.c_ulong(request)] + list(args)

    try:
        ioctl_fn = _get_ioctl_fn()
    except Exception as e:
        raise NotImplementedError(
            'Unable to get ioctl()-function from C library: {err}'.format(err=str(e)))

    res = ioctl_fn(*ioctl_args)
    if res < 0:
        err = ctypes.get_errno()
        raise OSError(err, os.strerror(err))
    return res


def nvme_submit_admin_passthru(fd, cmd):
    NVME_IOCTL_ADMIN_CMD = IOWR('N', 0x41, ctypes.sizeof(cmd))
    return ioctl(fd, NVME_IOCTL_ADMIN_CMD, ctypes.byref(cmd))


def nvme_get_log(fd, nsid, log_id, data):
    admin_cmd = NVMEPassthruCommand()
    admin_cmd.opcode = NVME_ADMIN_GET_LOG_PAGE
    admin_cmd.nsid = nsid
    admin_cmd.addr = ctypes.addressof(data)
    data_len = ctypes.sizeof(data)
    admin_cmd.data_len = data_len
    numd = (data_len >> 2) - 1
    numdu = numd >> 16
    numdl = numd & 0xffff
    admin_cmd.cdw10 = log_id | (numdl << 16)
    admin_cmd.cdw11 = numdu
    return nvme_submit_admin_passthru(fd, admin_cmd)

def nvme_identify_log(fd, nsid, log_id, data):
    admin_cmd = NVMEPassthruCommand()
    admin_cmd.opcode = NVME_ADMIN_IDENTIFY
    admin_cmd.nsid = nsid
    admin_cmd.addr = ctypes.addressof(data)
    data_len = ctypes.sizeof(data)
    admin_cmd.data_len = data_len
    admin_cmd.cdw10 = 0x01
    return nvme_submit_admin_passthru(fd, admin_cmd)


def get_smart_log(dev_name):
    try:
        fd = os.open(dev_name, os.O_RDONLY)
        smart_log = NVMESmartLog()
        ret = nvme_get_log(fd, NSID, NVME_LOG_SMART, smart_log)
    except Exception as ex:
        raise RuntimeError("can not get device smart log")
    finally:
        os.close(fd)
    return smart_log


def get_identify_log(dev_name):
    fd = None
    try:
        fd = os.open(dev_name, os.O_RDONLY)
        identify_log = NVMEIdCtrl()
        ret = nvme_identify_log(fd, NSID_ZERO, NVME_LOG_SMART, identify_log)
        if ret != 0:
            ret = nvme_identify_log(fd, NSID, NVME_LOG_SMART, identify_log)
    except Exception as ex:
        raise RuntimeError("can not get device identify log")
    finally:
        if fd:
            os.close(fd)
    return identify_log


def is_nvme_device(dev_path):
    dev_name = os.path.basename(dev_path)
    if dev_name.startswith('nvme'):
        return True
    return False


def get_nvme_identify_sn(dev_name):
    idtenfiy_logs = get_identify_log(dev_name)
    return idtenfiy_logs.sn.strip()


def get_nvme_identify_mn(dev_name):
    idtenfiy_logs = get_identify_log(dev_name)
    return idtenfiy_logs.mn.strip()


def convert_c_array_to_dec_value(array, base, length):
    value = 0
    for i in range(0, length):
        value += array[i] * (base**i)
    return value


class NVMESmartInfo:
    def __init__(self, dev_name):
        self.smart_logs = get_smart_log(dev_name)

    def get_nvme_media_errors(self):
        value = convert_c_array_to_dec_value(self.smart_logs.media_errors, 256, 16)
        return value

    def get_nvme_percentage_used(self):
        return self.smart_logs.percent_used


def nvme_smart_warn(dev_name):
    nvme_smart_info = NVMESmartInfo(dev_name)
    nvme_percentage_used_value = 100 - nvme_smart_info.get_nvme_percentage_used()
    nvme_media_error_value = nvme_smart_info.get_nvme_media_errors()
    if nvme_media_error_value > 0:
        return "ERROR"
    elif nvme_percentage_used_value < 15:
        return "ERROR"
    elif nvme_percentage_used_value < 30:
        return "WARNING"
    else:
        return "OK"


def is_optane(dev_path):
    INTEL_OPTANE_PCIID = ('0x8086', '0x2701')

    path = '/sys/block/{}/device/device/'.format(os.path.basename(dev_path))
    if not os.path.exists(path):
        return False
    try:
        vendor = do_cmd('cat {}/vendor'.format(path)).strip()
        device = do_cmd('cat {}/device'.format(path)).strip()
        return INTEL_OPTANE_PCIID == (vendor, device)
    except:
        return False



def get_disk_path_of_first_vd(adapter=None, pci_info=None):

    if adapter is None:
        adapter = '0'

    options = '-LdInfo -lALL -a{}'.format(adapter)
    vds = LdParser(options=options).get_formatted_data()
    if not vds:
        raise NoVdExists

    disk_path = None
    for vd in vds:
        disk_path = target_id_to_disk(
            vd['target_id'], adapter, pci_info=pci_info)
        if disk_path:
            break

    return disk_path


SSD_INDICATOR = {
    'INTEL': '233',
    'INDILINX': '209',
    'MICRON': '202',
    'SAMSUNG': '177',
    'SANDFORCE': '231',
    'SANDISK': '178'
}


def get_ssd_indicator_attr(model):
    if not model:
        logger.warning("Empty model, default SSD indicator is returned")
        return None

    disk_model = model.upper()
    for model, attr in SSD_INDICATOR.items():
        if model in disk_model:
            if not attr:
                print_err("Cannot find SSD indicator for '{}', please conduct manual inspection".format(model))
            return attr
    logger.debug(
        "Cannot find SSD indicator for '%s', default SSD indicator is returned", model)
    return None


def _get_smart_attrs(disk, raid=False, dev_id=None, ssd_indicator=None, sas=False):
    ret = [None] * 256
    if sas:
        cmd = "smartctl -a {} -j".format(disk)
        if raid:
            cmd = "smartctl -a -d megaraid,{} {} -j".format(dev_id, disk)
        try:
            lines = do_cmd(cmd)
            lines = json.loads(lines)
        except DoCommandError as e:
            try:
                lines = os.popen(cmd)
                lines = lines.read()
                lines = json.loads(lines)
            except Exception as e:
                print("{} exit code {}, it does not matter, error detail {}{}".format(cmd, e.errno, e.stderr, e.stdout))
                return ret
        except DoCommandTimedOut as e:
            logger.warn("{} timeout".format(cmd))
            return ret
        except Exception as e:
            raise e
        if int(lines['scsi_grown_defect_list']) > 0:
            ret[255] = int(lines['scsi_grown_defect_list'])
            return ret
        elif int(lines['scsi_error_counter_log']['read']['total_uncorrected_errors']) > 0:
            ret[255] = int(lines['scsi_error_counter_log']['read']['total_uncorrected_errors'])
            return ret
        elif int(lines['scsi_error_counter_log']['write']['total_uncorrected_errors']) > 0:
            ret[255] = int(lines['scsi_error_counter_log']['write']['total_uncorrected_errors'])
            return ret
    cmd = "smartctl -A {}".format(disk)
    if raid:
        cmd = "smartctl -A -d megaraid,{} {}".format(dev_id, disk)
    try:
        lines = do_cmd(cmd).strip().split('\n')
    except DoCommandError as e:
        logger.debug(
            "{} exit code {}, it does not matter".format(cmd, e.errno))
        lines = e.stdout.strip().split('\n')
    except DoCommandTimedOut as e:
        logger.warn("{} timeout".format(cmd))
        return ret
    except Exception as e:
        raise e
    start_getattr = False
    if ssd_indicator:
        ssd_indicators = [ssd_indicator]
    else:
        ssd_indicators = guess_ssd_indicator_attr()
    for line in lines:
        if start_getattr:
            info = line.split()
            attr_id = info[0]
            # get value(3) for ssd_indicator, get raw(9) for others
            attr_val = info[3] if attr_id in ssd_indicators else info[9]
            # some raw value's format is 0/0
            ret[int(attr_id)] = int(attr_val) if attr_val.isdigit() else None
        if "ID" in line:
            # attributes info are below `ID# ATTRIBUTE_NAME FLAG ...`
            start_getattr = True
    return ret


MOCK_SMART = "/etc/ezs3/mock_smart"


def extract_smart_attrs_for_raid(disk, dev_id, ssd=False, model=None, sas=False):

    if os.path.exists(MOCK_SMART):
        return get_mock_states(disk)

    ret = {}
    ssd_indicator = get_ssd_indicator_attr(model) if ssd else None
    try:
        attrs = _get_smart_attrs(
            disk, True, dev_id, ssd_indicator=ssd_indicator, sas=sas)
    except Exception as ex:
        logger.exception('Failed to extract SMART attribute')
        ret[-1] = 'Failed to extract SMART attribute: {}'.format(ex)
        return ret

    ret = _extract_smart_attrs(attrs, ssd, ssd_indicator)
    return ret


def guess_ssd_indicator_attr():
    return ["233", "173"]


class DiskHealthIndicator:
    SMART_5 = "REALLOCATED_SECTOR_COUNTER"
    SMART_187 = "REPORTED_UNCORRECTABLE_ERRORS"
    SMART_188 = "COMMAND_TIMEOUT"
    SMART_197 = "CURRENT_PENDING_SECTORS"
    SMART_198 = "UNCORRECTABLE_SECTORS"
    SMART_SSD_WEAROUT = "MEDIA_WEAROUT_INDICTOR"
    SMART_NVME_MEDIA_ERROR = "NVME_MEDIA_AND_DATA_INTEGRITY_ERROR"
    SMART_SAS_GROWN_DEFECT = "ELEMENTS_IN_GROWN_DEFECT_LIST"
    SMART_SAS_RW_ERROR = "SMART_SAS_RW_ERROR"


def _extract_smart_attrs(attrs, ssd=False, ssd_indicator=None):
    result = {}
    if ssd:
        if ssd_indicator is None:
            logger.warning(
                'ssd_indicator should not be None, use default value')
            ssd_indicators = guess_ssd_indicator_attr()
        else:
            ssd_indicators = [ssd_indicator]

        for ssd_indicator in ssd_indicators:
            if attrs[int(ssd_indicator)] is not None:
                result[DiskHealthIndicator.SMART_SSD_WEAROUT] = int(
                    attrs[int(ssd_indicator)])
                break

    elif False:
        if attrs[5] is not None:
            result[DiskHealthIndicator.SMART_5] = int(attrs[5])
        if attrs[187] is not None:
            result[DiskHealthIndicator.SMART_187] = int(attrs[187])
        # if attrs[188] is not None:
        #     result[DiskHealthIndicator.SMART_188] = int(attrs[188] % 0x10000)
        if attrs[197] is not None:
            result[DiskHealthIndicator.SMART_197] = int(attrs[197])
        if attrs[198] is not None:
            result[DiskHealthIndicator.SMART_198] = int(attrs[198])
    if attrs[255] is not None:
        result[DiskHealthIndicator.SMART_SAS_GROWN_DEFECT] = int(attrs[255])
    return result


def get_mock_states(disk):
    ret = {}
    states = None
    # mock_smart format: {"$disk_path":{"$attr_name":"$attr_val"}}
    with open(MOCK_SMART, 'r') as fp:
        try:
            content = fp.read()
            states = json.loads(content) if content else None
        except Exception:
            logger.exception('Detect mock smart data, but fail to load it')

    if states and disk in states.keys():
        ret = {key: int(val) for key, val in states[disk].iteritems()}
    return ret


class DiskErrorEvent:
    # error code < 0 -> general error
    FAIL_TO_GET_SMART_ATTRIBUTES = -1
    FAIL_TO_EVALUATE_SMART_EVENT = -2
    # error code from 1 to 256 -> smart error
    HAS_REALLOCATED_SECTORS = 5
    REPORTED_UNCORRECTABLE_ERRORS = 187
    COMMAND_TIMEOUT = 188
    HAS_PENDING_SECTORS = 197
    HAS_UNCORRECTABLE_SECTORS = 198
    DISK_WORN_OUT = 233
    SAS_ELEMENTS_IN_GROWN_DEFECT_LIST = 255
    # error code > 1000 -> other error event
    ROOT_PARTITION_FULL = 1000


class DiskWarningEvent:
    SMART_DATA_NOT_AVAILABLE = 0
    # warning code from 1 to 256 -> smart warning
    TEMPERATURE_ABNORMAL = 194
    DISK_NEARLY_WEAR_OUT = 233
    # warning code > 1000 -> other warning event
    ROOT_PARTITION_NEARLY_FULL = 1000


def check_smart_events(smart_states):
    ret = {}
    errors = {}
    warns = {}
    info = {}
    try:
        for smart_attr, attr_value in smart_states.iteritems():
            if smart_attr == -1:
                info[DiskErrorEvent.FAIL_TO_GET_SMART_ATTRIBUTES] = attr_value
            elif smart_attr == DiskHealthIndicator.SMART_5:
                if attr_value > 10:
                    warns[DiskErrorEvent.HAS_REALLOCATED_SECTORS] =\
                        'Reallocated Sectors Count: {}'.format(attr_value)
                elif attr_value > 0:
                    info[DiskErrorEvent.HAS_REALLOCATED_SECTORS] =\
                        'Reallocated Sectors Count: {}'.format(attr_value)
            elif (smart_attr == DiskHealthIndicator.SMART_187 and
                    attr_value > 0):
                warns[DiskErrorEvent.REPORTED_UNCORRECTABLE_ERRORS] =\
                    'Reported Uncorrectable Errors: {}'.format(attr_value)
            elif smart_attr == DiskHealthIndicator.SMART_188:
                if attr_value > 10:
                    warns[DiskErrorEvent.COMMAND_TIMEOUT] =\
                        'Command Timeout: {}'.format(attr_value)
                elif attr_value > 0:
                    info[DiskErrorEvent.COMMAND_TIMEOUT] =\
                        'Command Timeout: {}'.format(attr_value)
            elif (smart_attr == DiskHealthIndicator.SMART_197 and
                    attr_value > 0):
                warns[DiskErrorEvent.HAS_PENDING_SECTORS] =\
                    'Current Pending Sector: {}'.format(attr_value)
            elif (smart_attr == DiskHealthIndicator.SMART_198 and
                    attr_value > 0):
                warns[DiskErrorEvent.HAS_UNCORRECTABLE_SECTORS] =\
                    'Uncorrectable Sector Count: {}'.format(attr_value)
            elif smart_attr == DiskHealthIndicator.SMART_SSD_WEAROUT:
                if attr_value <= 15:
                    errors[DiskErrorEvent.DISK_WORN_OUT] =\
                        'Disk worn out, wear out level is {}'.format(
                            attr_value)
                elif (attr_value > 15 and
                        attr_value <= 25):
                    warns[DiskWarningEvent.DISK_NEARLY_WEAR_OUT] =\
                        'Disk nearly wear out, wear out level is {}'.format(
                            attr_value)
            elif smart_attr == DiskHealthIndicator.SMART_SAS_GROWN_DEFECT:
                if attr_value > 0:
                    warns[DiskErrorEvent.SAS_ELEMENTS_IN_GROWN_DEFECT_LIST] =\
                        'Elements in grown defect list: {}'.format(attr_value)

    except Exception as ex:
        logger.exception('Failed to evaluate SMART error events')
        errors[DiskErrorEvent.FAIL_TO_EVALUATE_SMART_EVENT] =\
            'Failed to evaluate SMART error events: {}'.format(ex)

    ret = {'error_events': errors, 'warning_events': warns, 'info_events': info,
           'not_supported': True if 0 in smart_states else False}
    return ret


class NoVdExists(Exception):
    pass


def check_raid_disk_smart_states(adapter, vd_num, pd, disk=None, pci_info=None):
    if not disk:
        if adapter and vd_num:
            disk = target_id_to_disk(vd_num, adapter, pci_info=pci_info)
        else:
            try:
                disk = get_disk_path_of_first_vd(adapter, pci_info=pci_info)
            except NoVdExists:
                disk = None

    logger.debug("disk {} device_id {}".format(disk, pd['device_id']))
    if disk is None:
        print('Failed to get disk path of vd [{}], adapter [{}]'
                     .format(vd_num, adapter))
        return None

    try:
        is_ssd = pd['media_type'] == 'SSD'
        is_sas = pd['pd_type'] == 'SAS'
        attrs = extract_smart_attrs_for_raid(
            disk, dev_id=pd['device_id'], ssd=is_ssd, model=pd['inquiry_data'], sas=is_sas)
        result = check_smart_events(attrs)
        return result
    except Exception as e:
        logger.exception("failed to parse smartctl output. {}"
                         .format(e.message))
        return {}


class BThreadPool(Pool):
    """
        almost same as multiprocessing.ThreadPool, except that:
        BThreadPool support maxtasksperchild
    """
    from multiprocessing.dummy import Process

    def __init__(self, processes=None, initializer=None, initargs=(), maxtasksperchild=None):
        Pool.__init__(self, processes, initializer, initargs, maxtasksperchild)

    def _setup_queues(self):
        self._inqueue = Queue.Queue()
        self._outqueue = Queue.Queue()
        self._quick_put = self._inqueue.put
        self._quick_get = self._outqueue.get

    @staticmethod
    def _help_stuff_finish(inqueue, task_handler, size):
        # put sentinels at head of inqueue to make workers finish
        inqueue.not_empty.acquire()
        try:
            inqueue.queue.clear()
            inqueue.queue.extend([None] * size)
            inqueue.not_empty.notify_all()
        finally:
            inqueue.not_empty.release()


class ThreadPool(object):
    THREAD_NUM = 42
    LOCK = threading.Lock()
    tpool = None

    def __new__(cls):
        with cls.LOCK:
            if cls.tpool is None:
                cls.tpool = BThreadPool(cls.THREAD_NUM, maxtasksperchild=50)
        return super(ThreadPool, cls).__new__(cls)

    def run_jobs(self, func, args_list, kwargs_list=[]):
        if len(kwargs_list) > 0 and len(args_list) != len(kwargs_list):
            raise RuntimeError('args not valid')
        if len(kwargs_list) == 0:
            kwargs_list = [{} for _ in range(len(args_list))]
        results = [self.tpool.apply_async(
            func, args, kwargs) for args, kwargs in zip(args_list, kwargs_list)]
        ret = []
        for r in results:
            ret.append(r.get())
        return ret


class LSI_RAID():

    def get_ldpd_info(self, format_rules=None, details=None):
        data = LdPdParser().get_formatted_data(format_rules=format_rules)
        if not data:
            return []

        if details:

            pciinfo = AdapterPciInfoParser().get_formatted_data()
            results = ThreadPool().run_jobs(
                get_ldpd_details,
                [(details, vd['target_id'], vd['adapter'], vd['vd'], pd)
                 for vd in data for pd in vd['pds']],
                [{'pci_info': pciinfo} for vd in data for _ in vd['pds']]
            )
            for i, vd in enumerate(data):
                for j in range(len(vd['pds'])):
                    ret = results.pop(0)
                    data[i].update({'disk': ret['disk']})
                    data[i]['pds'][j].update(
                        {k: ret[k] for k in ['healthy', 'lifetime_remaining', 'rebuild_progress', 'disk_events']})
        return data

    def get_unassigned_pds(self, format_rules=None, details=None):
        pds = PdParser().get_formatted_data(format_rules=format_rules)
        if details and (LdpdDetailType.DISK_EVENTS & details):
            adapter = None
            disk = None

        unassigned_pds = filter(
            lambda pd: pd['drive_position'] is None and 'jbod' not in pd["firmware_state"].lower(
            ),
            pds
        )

        for pd in unassigned_pds:
            if pd["type"] and 'Hotspare' in pd["firmware_state"]:
                if pd['array']:
                    pd["firmware_state"] = pd["type"] + \
                        " Hotspare (VD: " + pd["array"] + ")"
                else:
                    pd["firmware_state"] = pd["type"] + " Hotspare"
            if 'copyback' in pd["firmware_state"].lower():
                pd['copyback_progress'] = get_pd_copyback_progress(
                    pd['enclosure'], pd['slot'], pd['adapter'])
            else:
                pd['copyback_progress'] = None

            if details and (LdpdDetailType.DISK_EVENTS & details):
                # get a disk path and pass it to check_raid_disk_smart_states() later
                # to reduce calls of get_disk_path_of_first_vd() if "adapter" does not change
                if adapter != pd['adapter']:
                    adapter = pd['adapter']
                    try:
                        disk = get_disk_path_of_first_vd(adapter)
                    except NoVdExists:
                        disk = None
                pd['disk_events'] = check_raid_disk_smart_states(
                    adapter, None, pd, disk)

        return unassigned_pds


def get_pd_copyback_progress(enclosure, slot, adapter):
    cmd = '{} -PDCpyBk -ShowProg -PhysDrv[{}:{}] -a{}'.format(
        RAID_CLI, enclosure, slot, adapter
    )
    result = do_cmd(cmd, timeout=5, force=True)
    logger.info('get_pd_copyback_progress : %s', result)
    # e.g. Rebuild Progress on Device at Enclosure 24, Slot 3 Completed 1% in 3 Minutes.
    r = PROGRESS_REGEX.search(result) if result else None
    if r:
        copyback_progress = '{} Completed'.format(r.group(1))
    else:
        copyback_progress = None

    return copyback_progress


def check_disk_health():
    result = {}
    raid_disks = get_ldpd_info(details=LdpdDetailType.VD_DISK_NAME)
    raid_disk_names = [vd["disk"] for vd in raid_disks]
    unassigned_disk = get_unassigned_pds()

    all_disks = scan_disks()
    onboard_disks = filter(
        lambda d: d["name"] not in raid_disk_names
        and not d["name"].startswith("/dev/bus"),
        all_disks,
    )
    
    for disk in onboard_disks:
        disk_info = parse_disk_info(disk["name"], disk["type"])
        try:
            disk_info = parse_smart_info(disk["name"], disk["type"], disk_info)
        except:
            print("can't get smart info for disk, name: {}, type:{}, disk_info:{}".format(disk["name"], disk["type"], disk_info))
            continue
        result[disk["name"]] = disk_info

    for vd in raid_disks:
        disk = vd["disk"]
        info = []
        for pd in vd["pds"]:
            if not pd["device_id"]:
                continue
            disk_type = "megaraid,{}".format(pd["device_id"])
            disk_info = parse_disk_info(disk, disk_type)
            try:
                disk_info = parse_smart_info(disk, disk_type, disk_info)
            except:
                continue
            pd["adapter"] = vd["adapter"]
            disk_info["detail"] = pd
            info.append(disk_info)
        result[disk] = info

    if unassigned_disk:
        adp_disk_map = {}
        for vd in raid_disks:
            if vd["adapter"] not in adp_disk_map:
                adp_disk_map[vd["adapter"]] = vd["disk"]

        result["unassigned"] = []
        for pd in unassigned_disk:
            disk = adp_disk_map[pd["adapter"]]
            disk_type = "megaraid,{}".format(pd["device_id"])
            disk_info = parse_disk_info(disk, disk_type)
            try:
                disk_info = parse_smart_info(disk, disk_type, disk_info)
            except:
                continue
            disk_info["detail"] = pd
            result["unassigned"].append(disk_info)
    for disk in sorted(result):
        info = result[disk]
        if isinstance(info, list):
            # disk is a VD that contains several PDs
            # or 'unassigned' to indicate a group if unassigned disks
            for i in sorted(info, key=lambda i: int(i["detail"]["device_id"])):
                healthy, errors = is_healthy(i)
                name = (
                    "Unassigned disk"
                    if disk == "unassigned"
                    else "Disk {}".format(disk)
                )
                if healthy:
                    print_ok(
                        "{} (Adapter: {} Device ID: {}) is healthy.".format(
                            name,
                            i["detail"]["adapter"],
                            i["detail"]["device_id"],
                        )
                    )
                else:
                    print_err(
                        "{} (Adapter: {} Device ID: {}) is unhealthy.".format(
                            name,
                            i["detail"]["adapter"],
                            i["detail"]["device_id"],
                        ),
                        errors,
                    )
        else:
            healthy, errors = is_healthy(info)
            if healthy:
                print_ok("Disk {} is healthy".format(disk))
            else:
                print_err("Disk {} is unhealthy.".format(disk), errors)


def scan_disks():
    output = do_cmd("smartctl --scan-open --json=c")
    return json.loads(output)["devices"]


def parse_disk_info(disk_name, disk_type):
    try:
        output = do_cmd("smartctl -d {} -i {}".format(disk_type, disk_name))
    except DoCommandError as e:
        output = e.stdout + '\n' + e.stderr

    if "failed" in output or 'INQUIRY failed' in output:
        return {}

    info = {}
    for line in output.splitlines():
        try:
            key, value = line.split(":", 1)
        except ValueError:
            continue

        info[key] = value.strip()

    data = {}
    if "Device Model" in info and "satadom" in info["Device Model"].lower():
        data["disk_type"] = DiskType.SATA_DOM
        data["model"] = info["Model Number"]
    elif is_nvme_device(disk_name):
        data['disk_type'] = DiskType.NVME_SSD
        data['model'] = info["Model Number"]
    elif "Transport protocol" in info and info["Transport protocol"].startswith("SAS"):
        data["disk_type"] = DiskType.SAS_HDD
        data["model"] = info["Vendor"]
    elif "Rotation Rate" in info and info["Rotation Rate"] == "Solid State Device":
        data["disk_type"] = DiskType.SATA_SSD
        if "Model Family" in info and "SANDISK" in info["Model Family"].upper():
            data["model"] = info["Model Family"]
        else:
            data["model"] = info["Device Model"]
    else:
        data["disk_type"] = DiskType.SATA_HDD
        if "Device Model" in info:
            data["model"] = info["Device Model"]
        elif "Model Family" in info:
            data["model"] = info["Model Family"]
    return data


def parse_smart_info(disk_name, disk_type, disk_info):
    if not disk_info:
        return {}
    if disk_info["disk_type"] in (
        DiskType.SATA_DOM,
    ):
        return disk_info

    if disk_info["disk_type"] == DiskType.NVME_SSD:
        smart_log = get_smart_log(disk_name)
        disk_info.update(
            {
                "power_on_hours": convert_c_array_to_dec_value(
                    smart_log.power_on_hours, 256, 16
                ),
                "media_error": convert_c_array_to_dec_value(
                    smart_log.media_errors, 256, 16
                ),
                "health": 100 - smart_log.percent_used,
            }
        )
        return disk_info
    result = disk_type.split(",")
    if len(result) == 2:
        is_raid = True
        dev_id = result[1]
    else:
        is_raid = False
        dev_id = None
    
    ssd_indicator = None
    is_ssd = disk_info["disk_type"] == DiskType.SATA_SSD
    if is_ssd:
        ssd_indicator = get_ssd_indicator_attr(disk_info["model"])
        
    is_sas = disk_info["disk_type"] == DiskType.SAS_HDD
    attrs = _get_smart_attrs(disk_name, is_raid, dev_id,
                             ssd_indicator, sas=is_sas)
    
    if is_ssd:
        disk_info.update(
            {"power_on_hours": attrs[9], "health": attrs[int(ssd_indicator)]}
        )
    elif is_sas:
        disk_info.update(
            {"elements_in_grown_defect_list": attrs[255]}
        )
    else:
        disk_info.update(
            {
                "reallocated_sector": attrs[5],
                "reported_uncorrect": attrs[187],
                "current_pending_sector": attrs[197],
                "offline_uncorrectable": attrs[198],
            }
        )
    return disk_info


def is_healthy(disk_info):
    errors = []
    if "disk_type" not in disk_info:
        errors.append(Errors.SmartInfoError)
        return not errors, errors

    if disk_info["disk_type"] == DiskType.SATA_DOM:
        # SATA DOM disk is considered to be unhealthy because it seems to be unstable.
        return False, [Errors.SataDomError]
    if disk_info["disk_type"] == DiskType.SATA_SSD:
        if not disk_info["health"] > MIN_HEALTH_PERCENT:
            errors.append(Errors.DiskWearOut.format(
                100 - MIN_HEALTH_PERCENT, disk_info["health"]))
    elif disk_info["disk_type"] == DiskType.SAS_HDD:
        if disk_info["elements_in_grown_defect_list"] > 0:
            errors.append(Errors.SAS_GROWN_DEFECT.format(
                disk_info["elements_in_grown_defect_list"]))
    elif disk_info["disk_type"] == DiskType.NVME_SSD:
        if not disk_info["health"] > MIN_HEALTH_PERCENT:
            errors.append(Errors.DiskWearOut.format(
                100 - MIN_HEALTH_PERCENT, disk_info["health"]))
        if not disk_info["media_error"] == 0:
            errors.append(Errors.NvmeMediaError.format(
                disk_info["media_error"]))
    else:  # SATA_HDD
        if disk_info["reallocated_sector"] > 0:
            errors.append(Errors.Smart5Error.format(
                disk_info["reallocated_sector"]))

        if disk_info["reported_uncorrect"] > 0:
            errors.append(Errors.Smart187Error.format(
                disk_info["reported_uncorrect"]))

        if disk_info["current_pending_sector"] > 0:
            errors.append(Errors.Smart197Error.format(
                disk_info["current_pending_sector"]))

        if disk_info["offline_uncorrectable"] > 0:
            errors.append(Errors.Smart198Error.format(
                disk_info["offline_uncorrectable"]))

    return not errors, errors


def check_raid_states():
    raid_disks = get_ldpd_info(details=LdpdDetailType.VD_DISK_NAME)
    for raid in raid_disks:
        if "Degraded" in raid["state"]:
            print_err(
                "Raid (Adapter: {} VD: {}) is degraded.".format(
                    raid["adapter"],
                    raid["vd"],
                )
            )
        elif "Offline" in raid["state"]:
            print_err(
                "Raid (Adapter: {} VD: {}) is offline.".format(
                    raid["adapter"],
                    raid["vd"],
                )
            )
        else:
            print_ok(
                "Raid (Adapter: {} VD: {}) is OK.".format(
                    raid["adapter"],
                    raid["vd"],
                )
            )


NODE_TASKS = (
    {"desc": "Check Raid VD States", "handler": check_raid_states},
    {"desc": "Check Disk", "handler": check_disk_health},
)
CLUSTER_TASKS = ()


def check(node=False, cluster=False):
    utils = util()
    utils.check(node, cluster, NODE_TASKS, CLUSTER_TASKS, "Disk")


class util():
    def check(self, node, cluster, node_tasks, cluster_tasks, name):
        logger.info("Start checking %s...", name)
        if node:
            for idx, t in enumerate(node_tasks):
                print(t["desc"])
                t["handler"]()
                print("")

        if cluster:
            for idx, t in enumerate(cluster_tasks):
                print(t["desc"])
                t["handler"]()
                print("")


def fmt_msg(msg, errors):
    if not errors:
        return msg

    return "{}\n        {}".format(msg, "\n        ".join(errors))


def print_err(msg, details=None):
    message = "[ {}ERR{} ] {}".format(RED, NO_COLOR, fmt_msg(msg, details))
    # logger.error(message)
    print(message)


def print_ok(msg, details=None):
    message = "[ {}OK{} ] {}".format(GREEN, NO_COLOR, fmt_msg(msg, details))
    logger.info(message)
    print(message)


if __name__ == "__main__":
    # fmt: off
    parser = argparse.ArgumentParser(prog="hwcheck", description="A tool for checking hardware status")
    parser.add_argument("-c", "--cluster", action="store_true", help="Whether running cluster check or not")
    parser.add_argument("-n", "--node", action="store_true", help="Whether running node check or not")
    # fmt: on
    args = parser.parse_args()
    check(args.node, args.cluster)

