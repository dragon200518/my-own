#!/usr/bin/env python

## should put this file in /usr/local/bin/ ,and grant excute permission, and add cron in /etc/cron.d/ as following
## 0 02 * * 6  root python /usr/local/bin/cron_daemon.py >/dev/null 2>&1

import logging
import commands


logging.basicConfig(filename='/var/log/ezcloudstor/check_dpkg_new_version.log',format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S',level=logging.INFO)
logging.info('log begin.')


def get_all_deb_info():
    # exclude the ceph and ez*
    installed_deb_info={}
    query_result=commands.getoutput("dpkg -l |awk '{print $2,$3}' |grep -v ceph |grep -v ez").split("\n")
       

    logging.info('the query installed deb :{}'.format(query_result))
    for each in query_result:
        try:
            deb_name,deb_version=each.split(' ')
            installed_deb_info[deb_name]=deb_version
        except Exception as e:
            continue
            

        

    if installed_deb_info:
        return installed_deb_info
    else:
        logging.erro('can not query the installed deb info')


def check_deb_new_version(installed_deb_info):
    compare_result={}
    for each_deb_name in installed_deb_info.keys():
        try:
            query_deb_new_info=commands.getoutput("apt-get changelog {} |head -n 1 |awk '{{print $4}}'".format(deb_name)).split('\n')[0]
            if (installed_deb_info[each_deb_name]==query_deb_new_info):
                print "the deb {} has no newer version"

            else:
                compare_result[each_deb_name]=query_deb_new_info

        except Exception as e:
            logging.error("catch exception:{}".format(str(e)))
    return compare_result




if __name__ == '__main__':
    installed_deb_info=get_all_deb_info()

    result=check_deb_new_version(installed_deb_info)
    print result




