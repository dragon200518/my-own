package ErroHandle;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Logger;

class LoggingExcept extends Exception {
    private static Logger logger = Logger.getLogger("LoggingException");
    public LoggingExcept(){
        StringWriter trace = new StringWriter();
        printStackTrace(new PrintWriter(trace));

//        logger.severe(trace.toString());
        logger.info(trace.toString());
    }
}
public class LoggingException {
    public static void main(String[] args) {
        try{
            throw new LoggingExcept();
        } catch(LoggingExcept e) {
            System.out.println("Caught aaaaaa" + e);
        }
    }
}
