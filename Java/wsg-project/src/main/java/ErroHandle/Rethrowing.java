package ErroHandle;

public class Rethrowing {
    public static void f() throws Exception{
        System.out.println("Exception originate in f()");
        throw new Exception("throwing from f()");

    }
    public static void g() throws Exception{
        try{
            f();

        } catch(Exception e){
            System.out.println("Inside g(), e.printStatckTrace");
//            e.printStackTrace(System.out);
            e.printStackTrace();
            throw e;
        }
    }
    public static void h() throws Exception {
        try {
            f();
        } catch(Exception e) {
            System.out.println("Inside h(), e.printStackTrace");
            e.printStackTrace(System.out);
            throw (Exception) e.fillInStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            g();
        }catch(Exception e) {
            System.out.println("Main: printStackTrace()");
            e.printStackTrace();
        }

        try {
            h();
        }catch(Exception e) {
            System.out.println("Main for H(): printStackTrace()");
            e.printStackTrace();
        }
    }
}
