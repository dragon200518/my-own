package concurrency;

import java.lang.management.ThreadInfo;

public class MoreBasicThreads {
    public static void main(String[] args) {
//        for (int i=0;i<5;i++)
//            new Thread(new LiftOff()).start();
//
        for (int i=0;i<5;i++) {
            new Thread(new LiftOff()).start();
            System.out.println(" in for loop "+ i);
//            System.out.println(Thread.getAllStackTraces());

        }
        System.out.println("Waiting for LiftOff");
    }
}
