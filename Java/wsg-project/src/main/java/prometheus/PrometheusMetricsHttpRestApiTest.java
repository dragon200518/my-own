package prometheus;

import com.alibaba.fastjson.JSONObject;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

public class PrometheusMetricsHttpRestApiTest {

    /**
     * 读取时间，自定义默认8s，0表示没有超时时间
     */
    public static final int READ_TIMEOUT = 10000*8;

    /**
     * 连接时间，自定义默认8s，0表示没有超时时间
     */
    public static final int CONNECT_TIMEOUT = 10000*8;

    /**
     * 重试次数，自定义默认1
     */
    public static final int RETRY_COUNT = 1;

    static String url = "http://20.205.41.45:9090";
    static String promQL = "max(bookkeeper_server_ADD_ENTRY_REQUEST{job=\"bookie\", quantile=\"0.99\", success=\"true\"})";

    public static void main(String[] args) {
        JSONObject param = new JSONObject();
        param.put("query", promQL);
        String promURL = url + "/api/v1/query";
        try {
            String httpRet = postHttp(promURL, param, CONNECT_TIMEOUT,READ_TIMEOUT,RETRY_COUNT);
            System.out.println(httpRet);
        } catch (Exception e) {
            System.out.printf("prometheus query promURL[{}], promQL[{}],", promURL, promQL, e);
        }


    }
    /**
     * http 请求 POST
     *
     * @param url           地址
     * @param params        参数
     * @param connectTimeout 连接时间
     * @param readTimeout   读取时间
     * @param retryCount    重试机制
     * @return String 类型
     */
    public static String postHttp(String url, JSONObject params, int connectTimeout, int readTimeout, int retryCount) {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(connectTimeout);
        requestFactory.setReadTimeout(readTimeout);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(StandardCharsets.UTF_8)); // 设置编码集
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler()); // 异常处理
        MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<>();
        Set<String> keys = params.keySet();
        for (String key : keys) {
            paramMap.add(key, params.getString(key));
        }
        for (int i = 1; i <= retryCount; i++) {
            return restTemplate.postForObject(url, paramMap, String.class);
        }
        return null;
    }
}