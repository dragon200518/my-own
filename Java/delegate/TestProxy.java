package delegate;

import java.lang.reflect.InvocationHandler;

/**
 * 要代理的接口
 */
interface Hello {
    String say();
}

/**
 * 真实调用对象
 */
class RealHello {

    public String invoke(){
        return "i'm proxy";
    }
}

/**
 * JDK代理类生成
 */
class JDKProxy implements InvocationHandler {
    private Object target;

    JDKProxy(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] paramValues) {
        return ((RealHello)target).invoke();
    }
}

/**
 * 测试例子
 */
public class TestProxy {

    public static void main(String[] args){
        // 构建代理器
        JDKProxy proxy = new JDKProxy(new RealHello());
        ClassLoader classLoader = ClassLoaderUtils.getCurrentClassLoader();
        // 把生成的代理类保存到文件
System.setProperty("sun.misc.ProxyGenerator.saveGeneratedFiles","true");
        // 生成代理类
        Hello test = (Hello) Proxy.newProxyInstance(classLoader, new Class[]{Hello.class}, proxy);
        // 方法调用
        System.out.println(test.say());
    }
}