package delegate;
import java.lang.reflect.InvocationHandler;



public interface IObject {
    void request();
}

public class RealObject implements IObject {

    public void request() {
        // 模拟一些操作
        System.out.println("in RealObject ,func request");
       }
}

public class ObjProxyHandler implements InvocationHandler {
    IObject realT;

    public ObjProxyHandler(IObject t) {
        realT = t;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable {
        // request方法时，进行校验
        if (method.getName().equals("request") && !isAllow())
            return null;
        return method.invoke(realT, args);
    }

    /**
     * 模拟针对请求的校验判断
     */
    private boolean isAllow() {
        return false;
    }
}


public class 
public void startTest() {
    IObject proxy = (IObject) Proxy.newProxyInstance(
            IObject.class.getClassLoader(),
            new Class[]{IObject.class},
            new ObjProxyHandler(new RealObject()));
    proxy.request(); // ObjProxyHandler的invoke方法会被调用
}
